import React, { useContext, useEffect } from 'react'
import { useHistory } from "react-router-dom"
import ContextProvider, { AppContext } from './ContextProvider'
import PinGreen from "./assets/pin.svg"
import PinGray from './assets/pinGray.svg'
import PinRed from './assets/pinRed.svg'

const NavigationAdmin = (props) => {
    const history = useHistory()
    const context = useContext(AppContext)

    const onMouse = (placeId, action) =>
        context.setPlace(placeId, action)

    return (
        <div id="markers">
            {props.markers.map(({ _id, position, isHidden, isVerified }) =>
                <div key={_id} className="menuItem"
                    onClick={() => {
                        onMouse(_id, ContextProvider.ActionsEnum.CLICK)
                        history.push(`/admin/misto/${_id}`, {
                            place: position
                        })
                    }}
                    onMouseOver={() => { onMouse(_id, ContextProvider.ActionsEnum.OVER) }}
                    onMouseOut={() => { onMouse(_id, ContextProvider.ActionsEnum.OUT) }}
                >
                    <p>{position.name}, {position.city}</p>
                    <img src={isHidden ? PinGray : isVerified ? PinGreen : PinRed} />
                    <div className="clearfix"></div>
                </div>
            )}
        </div>
    )
}

export default NavigationAdmin
