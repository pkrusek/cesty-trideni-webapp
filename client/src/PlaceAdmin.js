import React, { useState, useEffect, useContext } from 'react'
import ContextProvider, { AppContext } from './ContextProvider'
import Button from '@material-ui/core/Button'
import { makeStyles, withStyles } from '@material-ui/core/styles'
//import {  } from '@material-ui/core'

const useStyles = makeStyles({
    greenButton: {
      background: '#32702F',
      border: 1,
      borderRadius: 3,
      color: '#FFFFFF',
      height: 48,
      width: '100%',
      '&:hover': {
        background: '#32702F',
      },
      margin: '0 0 5px 0',
    },
  })

const PlaceAdmin = (props) => {

    const classes = useStyles()
    const context = useContext(AppContext)
    const [city, setCity] = useState('')
    //let placeId = props.location.state._id

    useEffect(() => {
        console.log(props.location.state._id)
        
        setCity(props.location.state.place.city)
        return () => {
            //console.log('unmount')
            context.setPlace('', ContextProvider.ActionsEnum.BACK)
        }
      }, [])

    //console.log(props.location.state.place.city)
    //console.log(props)

    const lat = context.state.lat
    const lng = context.state.lng
    //console.log(context)

    return (
        <div id="markers">
            {/* <h2>Place {placeId} {lat} {lng}</h2> */}
            <div id="detailMarker"></div>
            {/* <ul>
                    <li v-for="option in options" v-bind:class="[option.isActive ? 'active' : 'noActive']"
                        @click="selectFilter(option, options)">{{ option.title }}</li>
                </ul> */}
            <Button variant="contained" className={classes.greenButton}>Uložit</Button>
            <Button variant="contained" className={classes.greenButton}>Odstranit</Button>
            <Button variant="contained" className={classes.greenButton}>Zrušit</Button>
        </div>
    )
}

export default PlaceAdmin
