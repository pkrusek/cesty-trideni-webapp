import React, { Component } from 'react'
import Axios from 'axios'
import NavigationAdmin from './NavigationAdmin'
import MapAdmin from './MapAdmin'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
} from "react-router-dom"
// import logo from './logo.svg';
import './css/App.css'
// import './css/normalize.css'
import Layout from './Layout'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid'
import { Hidden } from '@material-ui/core'
import About from './panels/About'
import ContextProvider from './ContextProvider'

export default class Admin extends Component {

    constructor(props) {
        super(props)

          this.state = {
            markers: []
          }

          this.fetchMarkers = this.fetchMarkers.bind(this)
    }

    async fetchMarkers(bounds, zoom) {
        console.log('FETCH')
        const southWest = bounds.getSouthWest()
        const northEast = bounds.getNorthEast()
        const { data } = await Axios.get(`${process.env.REACT_APP_API}/maps/markers/edit`, {
            params: {
                southWestLng: southWest.lng(),
                southWestLat: southWest.lat(),
                northEastLng: northEast.lng(),
                northEastLat: northEast.lat(),
                zoom: zoom
            }
        })
        this.setState({
            markers: data
        })
    }

    render() {
        return (
            <>
                 <CssBaseline />
                <ContextProvider>
                <Layout>
                    <Route exact path="/admin">
                        <NavigationAdmin markers={this.state.markers} />
                    </Route>
                    <MapAdmin
                        mapStyle={require('./staticData/styledMapType.json')}
                        markers={this.state.markers}
                        fetchMarkers={this.fetchMarkers}
                    />
                </Layout>
                </ContextProvider>
            </>
        )
    }
}
