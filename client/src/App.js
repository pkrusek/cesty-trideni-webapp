import React, { Component } from 'react'
import Axios from 'axios'
import Navigation from './Navigation'
import Map from './Map'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom"
// import logo from './logo.svg';
import './css/App.css'
// import './css/normalize.css'
import Layout from './Layout'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid'
import { Hidden } from '@material-ui/core'
import About from './panels/About'
import ContextProvider from './ContextProvider'

import Public from './Public'
import Admin from './Admin'

export default class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      isLoggedIn: false
    }
  }


  render() {

    if (this.state.isLoggedIn) {
      return (
        <>
          {/* <Route exact path="/admin"> */}
            <Admin />
          {/* </Route> */}
          <Redirect from="/" to="/admin" exact />
        </>
      )
    }
    return <Public />
  }
}
