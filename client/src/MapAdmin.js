import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import _ from 'lodash'
import ContextProvider, { AppContext } from './ContextProvider'
import Axios from 'axios'
import PinGreen from "./assets/pin.svg"
import PinGray from './assets/pinGray.svg'
import PinRed from './assets/pinRed.svg'
import SearchInput from './panels/SearchInput'
class MapAdmin extends Component {

    static contextType = AppContext

    map = null
    markers = []
    message = ''

    constructor(props) {
        super(props)

        this.state = {
            editingMode: false
        }
    }

    componentDidMount() {
        this.loadGoogleMapScript()

        if (window.navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                console.log(position.coords.latitude)
                console.log(position.coords.longitude)
                //position.coords.latitude, position.coords.longitude
                //this.getWeatherData(position.coords.latitude, position.coords.longitude)
            })
        }
    }

    componentDidUpdate() {
        const ctx = this.context
        if (this.message != ctx.state.message) {
            this.message = ctx.state.message
            console.log('UPDATE SEARCH')
            fetch(`${process.env.REACT_APP_API}/maps/search/${ctx.state.message}`)
                .then(res =>
                    res.json()
                )
                .then(json => {
                    const latLng = new window.google.maps.LatLng(json[1], json[0])
                    this.map.setZoom(16)
                    this.map.setCenter(latLng)
                    this.updateMap()
                    //window.google.maps.event.trigger(this.map, 'dragend')
                })
        }
        this.hitMarker()
    }

    loadGoogleMapScript() {
        if (window.google !== undefined) {
            return this.initMap()
        }

        const ApiKey = 'AIzaSyDy9oVwZda-37mV6WVGJ1qdVD8erpOftE8'

        const script = window.document.createElement('script')
        script.src = `https://maps.googleapis.com/maps/api/js?key=${ApiKey}&language=cs&libraries=geometry`
        script.async = true
        script.defer = true
        script.onerror = function () { window.alert("Selhalo načtení Google mapy.") }
        script.addEventListener('load', () => {
            this.initMap()
        });
        window.document.body.appendChild(script)
    }

    initMap() {
        const map = new window.google.maps.Map(document.getElementById('map'), {
            center: { lat: 50.073658, lng: 14.418540 },
            zoom: 16,
            minZoom: 14,
            controlSize: 28,
            mapTypeControl: true,
            clickableIcons: false,
            streetViewControl: false,
            options: {
                gestureHandling: 'greedy',
                fullscreenControl: false
            },
            mapTypeControlOptions: {
                mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map'],
                style: window.google.maps.MapTypeControlStyle.DEFAULT,
                position: window.google.maps.ControlPosition.TOP_LEFT
            }
        })
        this.map = map
        map.mapTypes.set('styled_map', new window.google.maps.StyledMapType(this.props.mapStyle, { name: 'Zelená mapa' }))
        map.setMapTypeId('styled_map')

        window.google.maps.event.addListenerOnce(map, 'idle', () => {
            this.updateMap()
        })
        map.addListener('dragend', () => {
            this.updateMap()
        })
        map.addListener('zoom_changed', () => {
            this.updateMap()
        })
    }

    async updateMap() {
        const lat = this.map.getCenter().lat()
        const lng = this.map.getCenter().lng()
        const search = this.props.location.search
        const currentSearch = "?" + new URLSearchParams({ lat: lat.toString(), lng: lng.toString(), zoom: this.map.getZoom() })
        if (search != currentSearch) {
            const { pathname } = this.props.location
            this.props.history.replace({
                pathname: pathname,
                search: currentSearch
            })
        }
        const ctx = this.context
        ctx.setCoordinates(lat, lng)

        await this.props.fetchMarkers(this.map.getBounds(), this.map.getZoom())
        this.addMarkers(this.props.markers)
    }

    hitMarker() {
        console.log('hit')
        const ctx = this.context
        const place = ctx.state.place
        const action = ctx.state.placeAction

        switch (action) {
            case ContextProvider.ActionsEnum.OVER:
                const marker = _.find(this.markers, { key: place })
                if (marker && marker.getAnimation() == null) {
                    marker.setAnimation(window.google.maps.Animation.BOUNCE)
                }
                break
            case ContextProvider.ActionsEnum.OUT:
                _.forEach(this.markers, match => {
                    match.setAnimation(null)
                })
                break
            case ContextProvider.ActionsEnum.CLICK:
                ctx.setPlace(place, ContextProvider.ActionsEnum.UNDEFINED)
                this.setState({
                    editingMode: true
                })
                this.map.setMapTypeId('satellite')
                this.map.heading = 90
                this.map.tilt = 45
                this.map.setZoom(20)
                break
            case ContextProvider.ActionsEnum.BACK:
                ctx.setPlace(place, ContextProvider.ActionsEnum.UNDEFINED)
                this.setState({
                    editingMode: false
                })
                this.map.setMapTypeId('styled_map')
                this.map.setZoom(16)
            default:
                console.log('unknown')
        }

    }

    addMarkers(markersData) {
        console.log("addMarkers")
        console.log(markersData)
        // ktere markery jsou na screenu? ty co nejsou, vyhodit 
        markersData.map(data => {
            const marker = createMarker(data)
            marker.setMap(this.map)
            this.markers.push(marker)
        })
        // _.forEach(markersData, data => {
        //     const marker = createMarker(data)
        //     marker.setMap(this.map)
        //     //this.markers().push(marker)
        //     //this.markers.push(marker)
        //     //match.setMap(null)
        // })
    }

    render() {
        return (
            <>
            <div id="panelWrapper">
                <SearchInput />
            </div>
            <div id="map">

            </div>
            </>
            // className={this.state.editingMode ? 'hidden' : ''}
        )
    }
}

function createMarker(markerData) {
    const icon = {
        url: markerData.isHidden ? PinGray : markerData.isVerified ? PinGreen : PinRed,
        anchor: new window.google.maps.Point(15, 30),
        scaledSize: new window.google.maps.Size(30, 30)
    }
    const marker = new window.google.maps.Marker({
        position: new window.google.maps.LatLng(markerData.lat, markerData.lng),
        icon: icon,
        draggable: false,
        optimized: false,
        clickable: true,
        zIndex: +new Date,
        key: markerData._id,
        zoom: 16
    })

    return marker
}

export default withRouter(MapAdmin)
