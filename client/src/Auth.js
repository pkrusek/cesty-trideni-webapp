import React, {useEffect} from 'react'
import {Backdrop, Fade, makeStyles, Modal} from "@material-ui/core"
import $ from "jquery";

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}))

const Auth = () => {
    const classes = useStyles()
    const [open, setOpen] = React.useState(false)

    useEffect(() => {
        handleOpen()
    }, [])

    const handleOpen = () => {
        setOpen(true)
    }

    const handleClose = () => {
        setOpen(false)
    }

    const loginForm = (
        <div className={classes.paper}>
            <h2 id="transition-modal-title">Text in a modal</h2>
            <p id="transition-modal-description">
                Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
            </p>
        </div>
    );

    return (
        <>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    {loginForm}
                </Fade>
            </Modal>
        </>
    )
}

export default Auth
