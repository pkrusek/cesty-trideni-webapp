const fs = require('fs')
const fastCsv = require('fast-csv')
const GeoHash = require('ngeohash')
const MarkerModel = require('../dist/models/marker.model.js')
const UserModel = require('../dist/models/user.model.js')
const mongoose = require('mongoose')
const Marker = mongoose.model('Marker')
const User = mongoose.model('User')

const stream = fs.createReadStream('./data/data.csv')

//mongoose.set('debug', true)

//mongoose.connect(`mongodb://localhost:27017/kamtridit`, { useNewUrlParser: true })
//mongoose.connect(`mongodb+srv://pavel_krusek:brZ0Z7E6WtRk6Q7n@cluster0.xwcax.mongodb.net/data?retryWrites=true&w=majority`, { useNewUrlParser: true })
mongoose.connect(`mongodb://localhost:27017/kamtridit`, { useNewUrlParser: true })

//User.findOne({ email: "admin@havas.cz" }).then((result) => {
    let modelData = []
    const csvStream = fastCsv.parse().on('data', (data) => {
        //console.log(data[2])
        const marker = new Marker()
        marker.lat = parseFloat(data[2])
        marker.lng = parseFloat(data[3])
        marker.location = [parseFloat(data[3]), parseFloat(data[2])]
        marker.geoHash = GeoHash.encode(parseFloat(data[2]), parseFloat(data[3]), 12)
        marker.cluster = {
                paper: parseInt(data[4]) > 0 ? true : false,
                plastic: parseInt(data[5]) > 0 ? true : false,
                glass: parseInt(data[6]) + parseInt(data[7]) + parseInt(data[8]) > 0 ? true : false,
                drinkCarton: parseInt(data[9]) > 0 ? true : false,
                metal: parseInt(data[10]) > 0 ? true : false,
                courtyard: (parseInt(data[12]) == 0 && parseInt(data[13]) == 0) ? false : true
            }
            marker.position = {
                city: data[0],
                name: data[1]
            }
            marker.isVerified = true
            marker.isHidden = false
            //marker._user = result
        modelData.push(
            marker
        //     {
        //     lat: parseFloat(data[2]),
        //     lng: parseFloat(data[3]),
        //     location: [parseFloat(data[3]), parseFloat(data[2])],
        //     geoHash: GeoHash.encode(parseFloat(data[2]), parseFloat(data[3]), 12),
        //     cluster: {
        //         paper: parseInt(data[4]) > 0 ? true : false,
        //         plastic: parseInt(data[5]) > 0 ? true : false,
        //         glass: parseInt(data[6]) + parseInt(data[7]) + parseInt(data[8]) > 0 ? true : false,
        //         drinkCarton: parseInt(data[9]) > 0 ? true : false,
        //         metal: parseInt(data[10]) > 0 ? true : false,
        //         courtyard: (parseInt(data[12]) == 0 && parseInt(data[13]) == 0) ? false : true
        //     },
        //     position: {
        //         city: data[0],
        //         name: data[1]
        //     },
        //     isVerified: true,
        //     isHidden: false,
        //     _userId: result._id
        // }
        )
    }).on('end', () => {
        Marker.insertMany(modelData, function (err, data) {
            console.log(err)
            //console.log(data)
            console.log('done')
        })
        // let user = User.findOne({ email: "havas@havas.cz" }).then((result) => {
        //     Marker.insertMany(modelData, function (err, data) {
        //         const markers = Marker.find()
        //         for (marker in markers) {
        //             //console.log(marker)
        //             //console.log(user)
        //             result.markersOwner.push(marker._id)
        //             result.save()
        //         }
        //         console.log('done')
        //         //console.log(`Inserted: ${data.insertedCount} rows`)
        //     })
        // })

        // for (marker in modelData) {
        //     marker.save()
        //     .then((result) => {
        //         user.markersOwner.push(marker)
        //         user.save()
        //     })
        // }


        // user.reviews.push(review);
        //             user.save();


    })

    stream.pipe(csvStream)
//})

