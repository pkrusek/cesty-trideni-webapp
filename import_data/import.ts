import fs from 'fs'
import fastCsv from 'fast-csv'
import GeoHash from 'ngeohash'
import Marker from '../src/models/marker.interface'
import MarkerModel from '../src/models/marker.model'
import mongoose from 'mongoose'

mongoose.connect(`mongodb://localhost:27017/kamtridit`, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})

const stream = fs.createReadStream('./data/data.csv')

let modelData: Marker[] = []
const csvStream = fastCsv.parse().on('data', (data) => {
    modelData.push(new MarkerModel({
        lat: parseFloat(data[2]),
        lng: parseFloat(data[3]),
        location: [parseFloat(data[3]), parseFloat(data[2])],
        geoHash: GeoHash.encode(parseFloat(data[2]), parseFloat(data[3]), 12),
        cluster: {
            paper: parseInt(data[4]) > 0 ? true : false,
            plastic: parseInt(data[5]) > 0 ? true : false,
            glass: parseInt(data[6]) + parseInt(data[7]) + parseInt(data[8]) > 0 ? true : false,
            drinkCarton: parseInt(data[9]) > 0 ? true : false,
            metal: parseInt(data[10]) > 0 ? true : false,
            courtyard: (parseInt(data[12]) == 0 && parseInt(data[13]) == 0) ? false : true
        },
        position: {
            city: data[0],
            name: data[1]
        },
        isVerified: false,
        isHidden: false
    }))
}).on('end', () => {
    MarkerModel.insertMany(modelData as [Marker], (data, err) => {
        if (err) throw err
        console.log(`Inserted: ${data.insertedCount} rows`)
    })
})

stream.pipe(csvStream)
