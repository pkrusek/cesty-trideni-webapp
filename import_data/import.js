const fs = require('fs')
const fastCsv = require('fast-csv')
const mongodb = require('mongodb')
const geoHash = require('ngeohash')

const MongoClient = mongodb.MongoClient
const uri = "mongodb+srv://pavel_krusek:brZ0Z7E6WtRk6Q7n@cluster0.xwcax.mongodb.net/data?retryWrites=true&w=majority"
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true })

const stream = fs.createReadStream('./data/data.csv')
let csvData = []
let csvStream = fastCsv.parse().on('data', (data) => {
    csvData.push({
        createdAt: new Date(),
        updatedAt: new Date(),
        geohash: geoHash.encode(parseFloat(data[2]), parseFloat(data[3]), 12),
        lat: parseFloat(data[2]),
        lng: parseFloat(data[3]),
        location: [parseFloat(data[3]), parseFloat(data[2])],
        cluster: {
            paper: parseInt(data[4]),
            plastic: parseInt(data[5]),
            glassMixture: parseInt(data[6]),
            glassClear: parseInt(data[7]),
            glassDuo: parseInt(data[8]),
            drinkCarton: parseInt(data[9]),
            metal: parseInt(data[10]),
            biowaste: parseInt(data[11])
        },
        clusterApi: {
            paper: parseInt(data[4]) == 0 ? false : true,
            plastic: parseInt(data[5]) == 0 ? false : true,
            glass: parseInt(data[6]) + parseInt(data[7]) + parseInt(data[8]) == 0 ? false : true,
            drinkCarton: parseInt(data[9]) == 0 ? false : true,
            metal: parseInt(data[10]) == 0 ? false : true,
            courtyard: (parseInt(data[12]) == 0 && parseInt(data[13]) == 0) ? false : true
        },
        isHidden: false,
        position: {
            city: data[0],
            name: data[1]
        }
    })
}).on('end', () => {
    client.connect(err => {
        if (err) throw err
        const collection = client.db('data').collection('markers')
        collection.insertMany(csvData, (err, res) => {
            if (err) throw err
            console.log(`Inserted: ${res.insertedCount} rows`)
            collection.createIndex({"location": "2d"}, (err, result) => {
                if (err) throw err
                console.log(result)
                collection.createIndex({"geohash": 1}, (err, result) => {
                    if (err) throw err
                    console.log(result)
                    client.close()
                })
            })
        })
    })
})

stream.pipe(csvStream)
