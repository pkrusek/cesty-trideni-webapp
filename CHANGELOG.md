# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] 31.07.2020

### Changed
- Mapa jako prvni polohu ukaze rovnou polohu uzivatele a ne centrum Prahy.
Centrum Prahy ukaze teprve, kdyz uzivatel nepovoli geolokaci.

## [0.2.0] 29.07.2020

### Added
- Clusterovani geodat na serveru pomoci geohashes.
- Cachovani clusteru a markeru v browseru pro lepsi uzivatelsky zazitek.
- Provozni podminky do sekce "O nas".

### Fixed
- Opravena rozbita hlavicka v mobilni verzi (portrait mod) na tabletech.
- Search autocomplete komponenta (hledani kontejneru) opraveny font, nyni odpovida stylu aplikace.
- V desktop i mobilni verzi overeno nastaveni, ze se neobjevi scroll, pokud neni treba.
- Oprava centrovani markeru na center point, takze pri zoomu se neposouvaji.
- Oprava sedeho ohraniceni (scrolly - jen nektere browsery).
- Opraven nadpis v detailu mista - srovnan s menu.
- Pres shluk se nyni uzivatel proklikne na detailnejsi zoom.
- Preformatovana sekce "O nas" podle grafickeho vzoru.

### Changed
- Filtrovani - prepsany princip podle pozadavku klienta. 
Nyni jsou aktivni vsechny markery, pokud splnuji aspon jedno filtrovaci kriterium.
- Ikona s lokaci uzivatele se ted animuje / pulzuje, takze je na mape snadno identifikovatelna.
- Kam patri - pro mobily mala abeceda a la iOS.
- Kam patri - na mobilni verzi nezmizi nadpis a tlacitko zpet.
- Kam patri - zmeneno prolinkovani - nyni se pouzije odkaz "samosebou" v pripade, ze existuje.
- Zmenen text ve vyhledavani a pridan text u vyhledavani do menu.


## [0.1.1] 23.07.2020

### Added
- Pridana sekce KAM PATRI.
- Pridan Changelog + cislo verze do navigacniho panelu.

### Fixed 
- Typo v detailu sberneho mista (kratke i ve slove papir).
- Datum v detailu sberneho mista predelany na cesky format (den, mesic, rok).  
Den a mesic v "kratkem formatu" bez leading zero.
- Opravena pozice titulky pro kazdy panel v desktop i mobilni verzi.

### Changed
- V mobilnim webu zkracen panel "Filtrovani" tak, aby byla videt pod panelem mapa.  
Samotne moznosti filtrovani jsou pak v mobile webu ve dvou sloupcich.
- Google places - hledani sberneho mista -  uprava query:
 1. hleda ted i cela mesta nebo obce
 2. preferuje ceske lokace

  
## [0.1.0] 21.07.2020

### Prvni verze projektu

<!-- ### Added
### Fixed
### Changed
### Deprecated
### Removed
### Security -->