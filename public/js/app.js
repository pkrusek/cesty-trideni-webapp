let map
let mapZoom = 17
let markers = []
let clusters = []
let selectedMarkers = new Set()
let infoWindow
let infoWindowDirections
let infoWindowClosest
let markersData = []
let filter = [false, false, false, false, false, false]

let pos
let locMarker
let meImage;
let me = "../assets/me.png"
let meMarker
let destinationImage
let destination = "../assets/destination.png"
let destinationMarker
let directionsService
let directionsRenderer
let startPart
let endPart

const svgAnimated = btoa([
    '<?xml version="1.0"?>',
    '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">',
    '<animate href="#back" attributeName="r" from="5.3" to="10" dur="1.2s" begin="0s" repeatCount="indefinite" fill="freeze" id="circ-anim"/>',
    '<animate href="#back" attributeType="CSS" attributeName="opacity" from="1" to="0" dur="1.2s" begin="0s" repeatCount="indefinite" fill="freeze" id="circ-anim" />',
    '<circle id="back" cx="10" cy="10" r="6.5" stroke-width="1" fill="MediumSeaGreen"/>',
    '<circle class="front" cx="10" cy="10" r="5.5" fill="MediumSeaGreen" />',
    '</svg>'
].join('\n'));

function initMap() {
    let styledMapType = new google.maps.StyledMapType(
        [
            {
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#c7e0e6"
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    },
                    {
                        "saturation": "-100"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#779977"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#d3e7d5"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#557744"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#c7e0e6"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#b6d4ae"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#b6d4ae"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#f0eb8c"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#f0eb8c"
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "557755"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#d5d5ee"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#96c6e7"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            }
        ], { name: 'Zelená mapa' });

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: mapZoom,
        controlSize: 28,
        mapTypeControl: true,
        clickableIcons: false,
        streetViewControl: false,
        options: {
            gestureHandling: 'greedy'
        },

        mapTypeControlOptions: {
            mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map'],
            style: google.maps.MapTypeControlStyle.DEFAULT,
            position: google.maps.ControlPosition.TOP_LEFT
        }
    });
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');

    var geolocationDiv = document.createElement('div');
    new GeolocationControl(geolocationDiv, map);

    directionsService = new google.maps.DirectionsService()

    map.addListener('dragend', () => {
        fetchMarkers()
    })

    map.addListener('zoom_changed', () => {
        if (infoWindow != null) {
            infoWindow.close(map)
            infoWindow = null
        }
        if (infoWindowClosest != null) {
            infoWindowClosest.close(map)
        }
        fetchMarkers()
    })


    const geocoder = new google.maps.Geocoder()

    map.addListener('click', (e) => {
        if (infoWindowClosest != null) {
            infoWindowClosest.close(map)
        }
        
        if (infoWindow != null) {
            infoWindow.close(map)
            infoWindow = null
        }

        // if (map.getZoom() >= 16) {
        //     geocoder.geocode({
        //         'latLng': e.latLng
        //     }, function(results, status) {
        //         if (status == google.maps.GeocoderStatus.OK) {
        //             if (results[0]) {
        //                 const str = results[0].formatted_address.replace(/,/g, '<br>')
        //                 infoWindowClosest = new google.maps.InfoWindow({
        //                     maxWidth: 250
        //                 })
        //                 infoWindowClosest.setContent(`${str}<br><br><img src="/assets/direction.svg" height="50" style="cursor: pointer;" onclick="directionToClosest(${e.latLng.lat()}, ${e.latLng.lng()})">`)
        //                 infoWindowClosest.setPosition(e.latLng)
        //                 infoWindowClosest.open(map)
        //             }
        //         }
        //     })
        // }
    })

    if (cookieName('noMarkers') === '') {
        document.cookie = `noMarkers=${false}; expires=${new Date(new Date().getTime()+60*60*1000*24*365).toGMTString()}; path=/;`
    }

    if (cookieName('cookiesFunctional') === '') {
        document.querySelector('#cookies-consent-general').setAttribute("class", "cookies-consent cookies-consent--active")
    }
    const latLng = new google.maps.LatLng(50.073658, 14.418540)
    map.setCenter(latLng)
    //geoLocate()
}

function directionToClosest(lat, lng) {
    let params = {
        "lat": lat,
        "lng": lng
    };
    let query = Object.keys(params)
        .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
        .join('&');
    let url = 'maps/marker/closest?' + query
    fetch(url)
        .then(response => response.json())
        .then(data => {
            calculateAndDisplayRoute(data.lat, data.lng, params)
        }).catch(error => {
        });
}

function cookieName(name) {
    var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'))
    if (match) {
        return match[2]
    } else {
        return ''
    }
}

function GeolocationControl(controlDiv, map) {

    var firstChild = document.createElement('button');
    firstChild.style.backgroundColor = '#fff';
    firstChild.style.border = 'none';
    firstChild.style.outline = 'none';
    firstChild.style.width = '28px';
    firstChild.style.height = '28px';
    firstChild.style.borderRadius = '2px';
    firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
    firstChild.style.cursor = 'pointer';
    firstChild.style.marginRight = '8px';
    firstChild.style.padding = '0';
    firstChild.title = 'Vaše poloha';
    controlDiv.appendChild(firstChild);

    var secondChild = document.createElement('div');
    secondChild.style.margin = '5px';
    secondChild.style.width = '18px';
    secondChild.style.height = '18px';
    secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-2x.png)';
    secondChild.style.backgroundSize = '180px 18px';
    secondChild.style.backgroundPosition = '6 6';
    secondChild.style.backgroundRepeat = 'no-repeat';
    firstChild.appendChild(secondChild);

    google.maps.event.addListener(map, 'center_changed', function () {
        secondChild.style['background-position'] = '0 0';
    });

    controlDiv.index = 1;
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);

    google.maps.event.addDomListener(firstChild, 'click', geoLocate);
}

function geoLocate() {
    infoWindow = new google.maps.InfoWindow;

    if (navigator.geolocation) {

        navigator.geolocation.getCurrentPosition(function (position) {

            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

            map.setCenter(pos);

            if (locMarker != null) {
                locMarker.setMap(null);
                locMarker = null;
            }

            locMarker = new google.maps.Marker({
                position: pos,
                map: map,
                title: '',
                label: '',
                clickable: false,
                icon:
                {
                    url: 'data:image/svg+xml;charset=UTF-8;base64,' + svgAnimated,
                    anchor: new google.maps.Point(25, 25),
                    scaledSize: new google.maps.Size(50, 50)
                },
                optimized: false,
                zIndex: 88
            });
            map.addListener('tilesloaded', () => {
                fetchMarkers()
            })
        }, function (err) {
            const latLng = new google.maps.LatLng(50.073658, 14.418540)
            map.setCenter(latLng)
            map.addListener('tilesloaded', () => {
                fetchMarkers()
            })
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        map.setCenter(latLng)
        map.addListener('tilesloaded', () => {
            fetchMarkers()
        })
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Bez povolené geolokace nepracuje tato funkce aplikace.' :
        'Váš browser nepodporuje geolokace.');
    infoWindow.open(map);
}

function switchPanel(el, pos, posMobile) {
    if (el === '#panelFilter') {
        const element = document.getElementById("panel");
        element.classList.add("panel-height-auto")
    }
    const media = window.matchMedia("(max-width: 768px)")
    if (media.matches) {
        document.querySelector(".panel__header").style.marginLeft = posMobile
    } else {
        document.querySelector(".panel__header").style.marginLeft = pos
    }
    document.querySelector('.panel__content').children[0].style.display = "none"

    document.querySelector(el).style.display = "block"
    document.querySelector('.panel__header').style.display = "block"
}

function switchBack() {
    let htmlCollection = document.querySelector('.panel__content').children
    let arr = Array.from(htmlCollection)
    arr.forEach(el => { el.style.display = "none" })
    document.querySelector('.panel__header').style.display = "none"
    document.querySelector('#mainMenu').style.display = "block"
    const element = document.getElementById("panel")
    element.classList.remove("panel-height-auto")
}

function joinUs() {

    let panel = document.getElementById('panel');
    panel.classList.toggle('panel--active');

    _.forEach(document.querySelector('.panel__content').children, function(div){
        div.style.display = "none"
    })

    _.forEach(document.querySelector('.panel__content').children, function(div){
        div.style.display = "none"
    })
    switchPanel('#panelUsers', '14%', '14%')
    document.querySelector('.panel__headerTitle').innerHTML = 'ZAPOJTE SE'
}

function legal() {
    document.querySelector('#panelAbout').style.display = "none"
    document.querySelector('#panelLegal').style.display = "block"
    document.querySelector(".panel__header").style.marginLeft = '5%'
    document.querySelector('.panel__headerTitle').innerHTML = 'PROVOZNÍ PODMÍNKY'
    document.querySelector('.panel__content').scrollTop = 0
}

function legalCookies() {
    let panel = document.getElementById('panel');
    panel.classList.toggle('panel--active');

    _.forEach(document.querySelector('.panel__content').children, function(div){
        div.style.display = "none"
    })
    switchPanel('#panelAbout', '14%', '14%')
    legal()
}

function moreInfo() {
    document.querySelector('#cookies-consent-general').removeAttribute("class", "cookies-consent--active")
    document.querySelector('#cookies-consent-settings').setAttribute("class", "cookies-consent cookies-consent--active")
}

function acceptAllCookies() {
    document.cookie = `cookiesFunctional=${true}; expires=${new Date(new Date().getTime()+60*60*1000*24*365).toGMTString()}; path=/;`
    document.cookie = `cookiesPerformance=${true}; expires=${new Date(new Date().getTime()+60*60*1000*24*365).toGMTString()}; path=/;`
    document.cookie = `cookiesSocial=${true}; expires=${new Date(new Date().getTime()+60*60*1000*24*365).toGMTString()}; path=/;`

    document.querySelector('#cookies-consent-general').removeAttribute("class", "cookies-consent--active")
}

function acceptCookies() {
    document.cookie = `cookiesFunctional=${true}; expires=${new Date(new Date().getTime()+60*60*1000*24*365).toGMTString()}; path=/;`
    if (document.getElementById('cookies-consent-settings-2').checked) {
        document.cookie = `cookiesFunctional=${true}; expires=${new Date(new Date().getTime()+60*60*1000*24*365).toGMTString()}; path=/;`
    } else {
        document.cookie = `cookiesFunctional=${false}; expires=${new Date(new Date().getTime()+60*60*1000*24*365).toGMTString()}; path=/;`
    }
    if (document.getElementById('cookies-consent-settings-3').checked) {
        document.cookie = `cookiesFunctional=${true}; expires=${new Date(new Date().getTime()+60*60*1000*24*365).toGMTString()}; path=/;`
    } else {
        document.cookie = `cookiesFunctional=${false}; expires=${new Date(new Date().getTime()+60*60*1000*24*365).toGMTString()}; path=/;`
    }
    document.querySelector('#cookies-consent-settings').removeAttribute("class", "cookies-consent--active")
}

function closeNoMarkers() {
    document.querySelector('#noMarkers').style.display = "none"
}

function fetchMarkers() {
    const bounds = map.getBounds()
    const southWest = bounds.getSouthWest()
    const northEast = bounds.getNorthEast()
    let params = {
        "southWestLng": southWest.lng(),
        "southWestLat": southWest.lat(),
        "northEastLng": northEast.lng(),
        "northEastLat": northEast.lat(),
        "zoom": map.getZoom()
    };
    let query = Object.keys(params)
        .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
        .join('&');
    let url = 'maps/markers?' + query
    fetch(url)
        .then(response => response.json())
        .then(data => {
            markersData = data
            addMarkers(data)
        }).catch(error => {
        });
}

function addMarkers(data) {
    const zoom = map.getZoom()

    if (zoom < 15) { //clusters
        _.forEach(markers, marker => {
            if (marker.markerZoom >= 16) {
                marker.setMap(null)
            }
        })
        // markers = []
        markers = _.remove(markers, function(n) {
            return n.markerZoom < 16
        })
        const iconGroup = {
            url: "../assets/cluster.svg",
            anchor: new google.maps.Point(30, 30),
            scaledSize: new google.maps.Size(60, 60)
        }
        _.forEach(clusters, cluster => {
            let clusterZoom = cluster.clusterZoom
            if (clusterZoom != zoom) {
                cluster.setMap(null)
            }
        })
        clusters = _.remove(clusters, function (n) {
            return n.clusterZoom === zoom
        })
        let points = []
        for (let i = 0; i < data.length; i++) {
            let item = data[i]
            if (item.pointsData) {
                for (let j = 0; j < item.pointsData.length; j++) {
                    points.push(item.pointsData[j])
                }
            } else if (item.count) {
                let marker = new google.maps.Marker({
                    position: new google.maps.LatLng(item.lat, item.lng),
                    draggable: false,
                    icon: iconGroup,
                    label: {
                        color: '#ffffff',
                        fontSize: '12px',
                        fontWeight: '900',
                        text: String(item.count),
                    },
                    optimized: false,
                    clickable: false,
                    zIndex: +new Date,
                    _id: item._id,
                    clusterZoom: zoom
                })
                if (!clusters.some(e => e._id == item._id)) {
                    clusters.push(marker)
                }
            }
        }
        for (var i = 0; i < clusters.length; i++) {
            clusters[i].setMap(map)
        }
        markersData = []
        markersData = points
        addCommonMarkers(points)
        _.forEach(markers, marker => {
            if (marker.markerZoom != zoom) {
                marker.setVisible(false)
            } else {
                marker.setVisible(true)
            }
        })
    } else { // markers
        for (var i = 0; i < clusters.length; i++) {
            clusters[i].setMap(null)
        }
        clusters = []
        addCommonMarkers(data)
    }
}

function addCommonMarkers(data) {
    const zoom = map.getZoom()
    const icon = {
        url: "../assets/pin.svg",
        anchor: new google.maps.Point(15, 30),
        scaledSize: new google.maps.Size(30, 30)
    }
    for (let i = 0; i < data.length; i++) {
        let item = data[i]
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(item.lat, item.lng),
            icon: icon,
            draggable: false,
            optimized: false,
            clickable: true,
            zIndex: +new Date,
            _id: item._id,
            markerZoom: zoom
        })
        marker.addListener('click', function () {
            showMarker(this._id, this)
        })
        if (!markers.some(e => e._id === item._id)) {
            markers.push(marker)
            marker.setMap(map)
        }
    }
    setTimeout(function(){ noMarkers() }, 500);
    setIcons()
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function noMarkers() {
    const cookie = cookieName('noMarkers')

    if (cookie === 'true' && getRandomInt(0, 7) != 1) {
        return
    }
    
    let count = 0
    for (let i = markers.length, bounds = map.getBounds(); i--;) {
        if (bounds.contains(markers[i].getPosition())) {
            count++
        }
    }
    if (map.getZoom() >= 16 && count == 0) {
        document.querySelector('#noMarkers').style.display = "block"
        if (cookie === 'false') {
            document.cookie = `noMarkers=${true}; expires=${new Date(new Date().getTime()+60*60*1000*24*365).toGMTString()}; path=/;`
        }
    } else {
        document.querySelector('#noMarkers').style.display = "none"
    }
}

function setIcons() {
    for (var i = 0; i < markersData.length; i++) {
        let item = markersData[i]

        const icon = {
            url: "../assets/pin.svg",
            anchor: new google.maps.Point(15, 30),
            scaledSize: new google.maps.Size(30, 30)
        }
        const iconOn = {
            url: "../assets/pinon.svg",
            anchor: new google.maps.Point(15, 30),
            scaledSize: new google.maps.Size(30, 30)
        }
        const iconOff = {
            url: "../assets/pinoff.svg",
            anchor: new google.maps.Point(15, 30),
            scaledSize: new google.maps.Size(30, 30)
        }

        let ic = icon
        if (selectedMarkers.has(item._id) == true) {
            ic = iconOn
        }

        const cluster = item.cluster
        const isGlass = cluster.glass
        const isPlace = cluster.courtyard

        const arr = [isGlass, cluster.plastic, cluster.paper, cluster.drinkCarton, cluster.metal, isPlace]
        for (let i = 0; i < arr.length; i++) {
            if (filter[i] == true) {
                if (arr[i] == false) {
                    ic = iconOff
                }
                if (arr[i] == true) {
                    ic = icon
                    break
                }
            }
        }

        if (isPlace == true) {
            ic = icon
        }

        let marker = markers.filter(function (e) {
            return e._id == item._id;
        })[0]
        marker.setIcon(ic)
        marker.setMap(map)
    }
}

function showMarker(mongoId, marker) {
    if (infoWindow != null) {
        infoWindow.close(map)
        infoWindow = null
    }

    if (infoWindowClosest != null) {
        infoWindowClosest.close(map)
    }

    const existIcon = marker.getIcon().url.split('/').pop()

    if (existIcon === "pin.svg") {
        const icon = {
            url: "../assets/pinon.svg",
            anchor: new google.maps.Point(15, 30),
            scaledSize: new google.maps.Size(30, 30)
        }
        marker.setIcon(icon)
    }

    selectedMarkers.add(marker._id)

    infoWindow = new google.maps.InfoWindow({
        maxWidth: 250
    })

    let url = `maps/marker/${mongoId}`
    fetch(url).then(response => response.text())
        .then(data => {
            infoWindow.setContent(data)
            infoWindow.open(map, marker)
        }).catch(error => {
        });
}

function findPlace(json) {
    const latLng = new google.maps.LatLng(json[1], json[0])
    map.setZoom(16)
    map.setCenter(latLng)

    google.maps.event.addListenerOnce(map, 'idle', function () {
        fetchMarkers()
    })
}

function filterMarkers(index, value) {
    filter[index] = value
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null)
    }
    fetchMarkers()
}

function direction(lng, lat) {
    navigator.geolocation.getCurrentPosition(function (position) {
        pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };

        meImage = new google.maps.MarkerImage(
            me,
            new google.maps.Size(32, 32),
            new google.maps.Point(0, 0),
            new google.maps.Point(16, 16))

        calculateAndDisplayRoute(lat, lng, pos)
    }, function () {
        handleLocationError(true, infoWindow, map.getCenter())
    });
}

function calculateAndDisplayRoute(lat, lng, position) {
    if (startPart != null) {
        clearRoute()
    }

    directionsRenderer = new google.maps.DirectionsRenderer({ suppressMarkers: true, polylineOptions: { strokeColor: '#00c000' } })
    directionsRenderer.setMap(map)

    thisContainer = {
        lat: lat,
        lng: lng
    };

    directionsService.route(
        {
            origin: position,
            destination: thisContainer,
            travelMode: 'WALKING'
        },

        function (response, status) {
            if (status === 'OK') {
                const centerPoint = Math.ceil(response.routes[0].legs[0].steps.length / 2)
                
                const latLng = new google.maps.LatLng(response.routes[0].legs[0].steps[centerPoint].start_location.lat(), 
                response.routes[0].legs[0].steps[centerPoint].start_location.lng())

                distance(position, {lat: lat, lng: lng}, latLng)

                directionsRenderer.setDirections(response)

                //draw missing parts at the start and the end
                var startLAT = response.routes[0].legs[0].start_location.lat();
                var startLNG = response.routes[0].legs[0].start_location.lng();
                var endLAT = response.routes[0].legs[response.routes[0].legs.length - 1].end_location.lat();
                var endLNG = response.routes[0].legs[response.routes[0].legs.length - 1].end_location.lng();

                // start part
                if (startPart != null) {
                    startPart.setMap(null);
                    startPart = null;
                }

                var lineSymbol = {
                    path: 'M 0,0 0,1',
                    strokeOpacity: 1,
                    strokeColor: '#00c000',
                    scale: 4
                };

                var startCoordinates = [
                    { lat: startLAT, lng: startLNG },
                    { lat: position.lat, lng: position.lng },

                ];

                startPart = new google.maps.Polyline({
                    path: startCoordinates,
                    strokeOpacity: 0.0,
                    geodesic: true,
                    strokeColor: '#00c000',
                    icons: [{
                        icon: lineSymbol,
                        fillColor: '#00c000',
                        offset: '12px',
                        repeat: '14px'
                    }]
                });

                startPart.setMap(map);

                // end part
                if (endPart != null) {
                    endPart.setMap(null);
                    endPart = null;
                }

                var lineSymbol = {
                    path: 'M 0,0 0,1',
                    strokeOpacity: 1,
                    strokeColor: '#00c000',
                    scale: 4
                };

                // get end part coordinates
                var endCoordinates = [
                    { lat: endLAT, lng: endLNG },
                    { lat: lat, lng: lng },

                ];

                // draw end part
                endPart = new google.maps.Polyline({
                    path: endCoordinates,
                    geodesic: true,
                    strokeOpacity: 0.0,
                    strokeColor: '#00c000',

                    icons: [{
                        icon: lineSymbol,
                        fillColor: '#00c000',
                        offset: '12px',
                        repeat: '14px'
                    }],

                });

                endPart.setMap(map);

                //end icon
                destinationImage = new google.maps.MarkerImage(
                    destination,
                    new google.maps.Size(16, 16),
                    new google.maps.Point(0, 0),
                    new google.maps.Point(8, 8));

                if (destinationMarker != null) {
                    destinationMarker.setMap(null);
                    destinationMarker = null;
                }

                destinationMarker = new google.maps.Marker({
                    position: endCoordinates[1],
                    map: map,
                    title: " ",
                    label: '',
                    icon: destinationImage,
                    optimized: false,
                    zIndex: 89
                });
            } else {
                handleLocationError(true, infoWindow, map.getCenter());
            }
        });
}

function clearRoute() {
    directionsRenderer.setMap(null)
    destinationMarker.setMap(null);
    destinationMarker = null;
    startPart.setMap(null);
    startPart = null;
    endPart.setMap(null);
    endPart = null;
}

function distance(from, to, latLng) {
    if (infoWindow != null) {
        infoWindow.close(map)
        infoWindow = null
    }

    const service = new google.maps.DistanceMatrixService()

    service.getDistanceMatrix({
            origins: [from],
            destinations: [to],
            travelMode: google.maps.TravelMode.WALKING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false,
        },(response, status) => {
            if (status !== "OK") {
                console.log("Error was: " + status)
            } else {
                // const originAddress = response.originAddresses[0]
                // const destinationAddress =  response.destinationAddresses[0]
                const distance = response.rows[0].elements[0].distance.text
                const duration = response.rows[0].elements[0].duration.text

                if (infoWindowDirections != null) {
                    infoWindowDirections.close(map)
                }
            
                infoWindowDirections = new google.maps.InfoWindow({
                    maxWidth: 250
                })

                google.maps.event.addListener(infoWindowDirections,'closeclick',function(){
                    if (startPart != null) {
                        clearRoute()
                    }
                })

                

                infoWindowDirections.setContent(`${distance}<br>${duration}`)
                infoWindowDirections.setPosition(latLng)
                infoWindowDirections.open(map)
            }
        }
    )
}
