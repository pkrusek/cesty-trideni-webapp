window.addEventListener("load", function () {
    const passwordForm = document.getElementById("passwordForm")
    passwordForm.addEventListener("submit", function () {
        resetPassword(passwordForm)
    })
})

function resetPassword(form) {
    if (form.psswd1.value != form.psswd2.value || form.psswd1.value.length < 8) {
        const el = document.querySelector("#responseBad")
        el.innerHTML = 'Hesla neodpovídají nebo nemají aspoň 8 znaků'
        el.style.display = "block"
        setTimeout(function () {
            el.style.display = 'none'
        }, 7000)
        return
    }
    const data = {
        password: form.psswd1.value,
        token: getParameterByName('token')
    }
    let url = 'api/auth/password/reset'
    fetch(url, {
        method: 'post',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then(async (response) => {
            if (response.status === 200) {
                const el = document.querySelector("#responseOK")
                el.innerHTML = 'Vaše heslo bylo resetováno.'
                el.style.display = "block"
            } else {
                let data = await response.json()
                const el = document.querySelector("#responseBad")
                el.innerHTML = data.message
                el.style.display = "block"
                setTimeout(function () {
                    el.style.display = 'none'
                }, 7000)
            }
        })
        .catch((error) => {
        })
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}