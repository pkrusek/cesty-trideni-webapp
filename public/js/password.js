window.addEventListener("load", function () {
    const passwordEmailForm = document.getElementById("passwordEmailForm")
    passwordEmailForm.addEventListener("submit", function () {
        checkEmail(passwordEmailForm)
    })
})

function checkEmail(form) {
    const data = {
        email: form.email.value
    }
    let url = 'api/auth/password'
    fetch(url, {
        method: 'post',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(async (response) => {
            if (response.status === 200) {
                const el = document.querySelector("#responseOK")
                el.innerHTML = 'Instrukce pro resetování hesla najdete v e-mailu'
                el.style.display = "block"
            } else {
                let data = await response.json()
                const el = document.querySelector("#responseBad")
                el.innerHTML = data.message
                el.style.display = "block"
                setTimeout(function () {
                    el.style.display = 'none'
                }, 7000)
            }
        })
        .catch((error) => {
        })
}
