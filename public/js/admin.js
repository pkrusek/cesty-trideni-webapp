let map
let markers = []
let editMode = false
let marker

function help() {
    document.querySelector(".overlayHelp").style.display = "block"
}

function closeHelp() {
    document.querySelector(".overlayHelp").style.display = "none"
}

window.addEventListener("load", function () {
    const loginForm = document.getElementById("loginForm")
    loginForm.addEventListener("submit", function () {
        login(loginForm)
    })
})

function login(form) {
    var un = form.username.value
    var pw = form.password.value
    var xmlhttp = new XMLHttpRequest()
    xmlhttp.open("post", "api/auth/login", true)
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8")
    xmlhttp.setRequestHeader("Device", "browser")
    xmlhttp.send(JSON.stringify({ "email": un, "password": pw }))
    xmlhttp.onreadystatechange = function () {
        loginResults(xmlhttp)
    }
}

function loginResults(xmlhttp) {
    if (xmlhttp.status == 200) {
        new Date(new Date().getTime()+60*60*1000*24).toGMTString()
        document.cookie = `user=${xmlhttp.response}; expires=${new Date(new Date().getTime()+60*60*1000*24*365).toGMTString()}; path=/;`
        loginForm.username.value = ""
        loginForm.password.value = ""
        document.querySelector(".overlay").style.display = "none"
        document.querySelector("#loginInfo").style.display = "block"
        fetchMarkers()
    } else {
        const loginForm = document.getElementById("loginForm")
        //loginForm.username.value = ""
        loginForm.password.value = ""
        const el = document.querySelector("#loginBad")
        el.style.display = "block"
        const json = JSON.parse(xmlhttp.response)
        el.innerHTML = json.message
        el.style.display = "block"
        setTimeout(function () {
            el.style.display = 'none'
        }, 7000)
    }
}

function logout() {

    var answer = window.confirm("Opravdu se chcete odhlásit?");
    if (answer) {
        document.cookie = "user=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
        let url = 'api/auth/logout'
        fetch(url, {
            method: 'post',
            headers: {
                'Device': 'browser'
            }
        })
            .then(async (response) => {
                if (response.status === 200) {
                    _.forEach(markers, match => {
                        match.setMap(null)
                    })
                    _.remove(markers)
                    vueMarkers.markers = []
                    document.querySelector("#loginInfo").style.display = "none"
                    fetchMarkers()
                } else {
                    //document.querySelector(".overlay").style.display = "block"
                }
            })
            .catch((error) => {
                //document.querySelector(".overlay").style.display = "block"
            })
    }
}

function cookieName(name) {
    var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'))
    if (match) {
        return match[2]
    } else {
        return ''
    }
}

function fetchMarkers() {
    if (editMode) {
        return
    }
    const bounds = map.getBounds()
    const southWest = bounds.getSouthWest()
    const northEast = bounds.getNorthEast()
    let params = {
        "southWestLng": southWest.lng(),
        "southWestLat": southWest.lat(),
        "northEastLng": northEast.lng(),
        "northEastLat": northEast.lat(),
        "zoom": map.getZoom()
    };
    let query = Object.keys(params)
        .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
        .join('&')
    let url = 'maps/markers/edit?' + query

    fetch(url)
        .then(async (response) => {
            let data = await response.json()
            if (response.status === 200) {
                addMarkers(data)
            } else {
                document.querySelector(".overlay").style.display = "block"
            }
        })
        .catch((error) => {
            document.querySelector(".overlay").style.display = "block"
        })
}

function addMarkers(data) {
    addCommonMarkers(data)
}

function addCommonMarkers(data) {
    const zoom = map.getZoom()
    const icon = {
        url: "../assets/pin.svg",
        anchor: new google.maps.Point(15, 30),
        scaledSize: new google.maps.Size(30, 30)
    }
    const iconRed = {
        url: "../assets/pinRed.svg",
        anchor: new google.maps.Point(15, 30),
        scaledSize: new google.maps.Size(30, 30)
    }
    const iconGray = {
        url: "../assets/pinGray.svg",
        anchor: new google.maps.Point(15, 30),
        scaledSize: new google.maps.Size(30, 30)
    }
    for (let i = 0; i < data.length; i++) {
        let item = data[i]
        let iconMap = icon
        if (!item.isVerified) {
            iconMap = iconRed
        }
        if (item.isHidden) {
            iconMap = iconGray
        }
        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(item.lat, item.lng),
            icon: iconMap,
            draggable: false,
            optimized: false,
            clickable: true,
            zIndex: +new Date,
            _id: item._id,
            clusterZoom: zoom
        })
        marker.addListener('click', function () {
            selectMarker(this)
        })
        if (!markers.some(e => e._id == item._id)) {
            markers.push(marker)
            marker.setMap(map)
        }
    }
    vueMarkers.markers = data
    vueUser.user = cookieName('user')
}

function findPlace(json) {
    const latLng = new google.maps.LatLng(json[1], json[0])
    map.setZoom(editMode ? 22 : 16)
    map.setCenter(latLng)

    google.maps.event.addListenerOnce(map, 'idle', function () {
        fetchMarkers()
        geocodeLatLng()
    })
}

function animateMarker(data) {
    const marker = _.find(markers, { _id: data._id })
    if (marker.getAnimation() == null) {
        marker.setAnimation(google.maps.Animation.BOUNCE)
    }
}

function deAnimateMarker() {
    _.forEach(markers, match => {
        match.setAnimation(null)
    })
}

function selectMarker(data) {
    console.log(data)
    let url = 'api/markers/' + data._id
    fetch(url)
        .then(async (response) => {
            let marker = await response.json()
            if (response.status === 200) {
                this.marker = marker
                getMarker(marker)
            } else {
                //document.querySelector(".overlay").style.display = "block"
            }
        })
        .catch((error) => {
            //document.querySelector(".overlay").style.display = "block"
        })
}

function getMarker(data) {
    document.querySelector('#markers').style.display = "none"
    document.querySelector('#newMarker').style.display = "none"
    document.querySelector('#panelWrapper').style.display = "none"
    document.querySelector('#marker').style.display = "block"
    document.querySelector('#newMarkerButton').style.display = "none"
    document.querySelector('#loginInfo').style.display = "none"
    
    vueMarkers.markers = []
    map.setMapTypeId('satellite')
    map.heading = 90
    map.tilt = 45
    document.querySelector("#center").style.display = "block"
    document.querySelector("#address").style.display = "inline-block"
    if (data.isVerified) {
        document.querySelector("#center").innerHTML = `<img src="assets/pinGreen.svg">`
        document.querySelector('#detailMarker').innerHTML = `<p class="title">${data.position.name}, ${data.position.city}</p><img src="assets/pinGreen.svg"><div class="clearfix"></div>`
    }
    if (!data.isVerified) {
        document.querySelector("#center").innerHTML = `<img src="assets/pinRed.svg">`
        document.querySelector('#detailMarker').innerHTML = `<p class="title">${data.position.name}, ${data.position.city}</p><img src="assets/pinRed.svg"><div class="clearfix"></div>`
    }
    if (data.isHidden) {
        document.querySelector("#center").innerHTML = `<img src="assets/pinGray.svg">`
        document.querySelector('#detailMarker').innerHTML = `<p class="title">${data.position.name}, ${data.position.city}</p><img src="assets/pinGray.svg"><div class="clearfix"></div>`
    }
    editMode = true
    _.forEach(markers, match => {
        match.setMap(null)
    })
    const marker = _.find(markers, { _id: data._id })
    map.setCenter(marker.getPosition())
    map.setZoom(20)
    _.remove(markers)

    vueMarker.options[0].isActive = data.cluster.glass
    vueMarker.options[1].isActive = data.cluster.plastic
    vueMarker.options[2].isActive = data.cluster.paper
    vueMarker.options[3].isActive = data.cluster.drinkCarton
    vueMarker.options[4].isActive = data.cluster.metal
    vueMarker.options[5].isActive = data.cluster.courtyard
}

function setMarkers(index, isActive) {
    if (index == 5) {
        this.marker.cluster.glass = isActive
        this.marker.cluster.plastic = isActive
        this.marker.cluster.paper = isActive
        this.marker.cluster.drinkCarton = isActive
        this.marker.cluster.metal = isActive
        this.marker.cluster.courtyard = isActive

        return
    }
    switch (index) {
        case 0:
            this.marker.cluster.glass = isActive
            break;
        case 1:
            this.marker.cluster.plastic = isActive
            break;
        case 2:
            this.marker.cluster.paper = isActive
            break;
        case 3:
            this.marker.cluster.drinkCarton = isActive
            break;
        case 4:
            this.marker.cluster.metal = isActive
            break;
        case 5:
            this.marker.cluster.courtyard = isActive
            break;
        default:
            console.log('unknown index')
    }
}

function editMarker() {
    let check = false
    _.each( this.marker.cluster, ( val, key ) => { 
        if (val == true) {
            check = true
        } 
    })
    if (check == false) {
        alert('Musíte označit aspoň jeden druh kontejneru')
        return
    }
    let url = 'api/markers'
    fetch(url, {
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'put',
        body: JSON.stringify(this.marker)
    })
        .then(async (response) => {
            let marker = await response.json()
            if (response.status === 200) {
                this.marker = null
                commonBack(19)
            } else {
                this.marker = null
                commonBack()
            }
        })
        .catch((error) => {
            this.marker = null
            commonBack()
        })
}

function deleteMarker() {
    let url = 'api/markers/report/' + this.marker._id
    fetch(url, {
        method: 'post'
    })
        .then(async (response) => {
            let marker = await response.json()
            if (response.status === 200) {
                this.marker = null
                commonBack()
            } else {
                this.marker = null
                commonBack()
            }
        })
        .catch((error) => {
            this.marker = null
            commonBack()
        })
}

function cancelEdit() {
    commonBack()
}

function commonBack(zoom = 16) {
    editMode = false
    document.querySelector('#loginInfo').style.display = "block"
    document.querySelector('#panelWrapper').style.display = "block"
    document.querySelector('#markers').style.display = "block"
    document.querySelector('#marker').style.display = "none"
    document.querySelector('#newMarkerButton').style.display = "block"
    map.setMapTypeId('styled_map')
    document.querySelector("#center").style.display = "none"
    document.querySelector("#address").style.display = "none"
    map.setZoom(zoom)
}

function newMarker() {
    this.marker = { position: { name: "", city: "" }, lat: 0, lng: 0, isVerified: true, isHidden: false, cluster: { glass: false, plastic: false, paper: false, drinkCarton: false, metal: false, courtyard: false } }
    document.querySelector('#markers').style.display = "none"
    document.querySelector('#newMarker').style.display = "block"
    document.querySelector('#marker').style.display = "none"
    document.querySelector('#newMarkerButton').style.display = "none"
    document.querySelector('#loginInfo').style.display = "none"

    vueMarkers.markers = []
    map.setMapTypeId('satellite')
    map.heading = 90
    map.tilt = 45
    document.querySelector("#center").style.display = "block"
    document.querySelector("#address").style.display = "inline-block"

    document.querySelector("#center").innerHTML = `<img src="assets/pinGreen.svg">`
    //document.querySelector('#detailMarker').innerHTML = `<p class="title">${data.position.name}, ${data.position.city}</p><img src="assets/pinGreen.svg"><div class="clearfix"></div>`

    editMode = true
    _.forEach(markers, match => {
        match.setMap(null)
    })
    _.remove(markers)

    map.setCenter(map.getCenter())
    map.setZoom(20)

    vueNewMarker.options[0].isActive = false
    vueNewMarker.options[1].isActive = false
    vueNewMarker.options[2].isActive = false
    vueNewMarker.options[3].isActive = false
    vueNewMarker.options[4].isActive = false
    vueNewMarker.options[5].isActive = false
}

function saveMarker() {
    let check = false
    _.each( this.marker.cluster, ( val, key ) => { 
        if (val == true) {
            check = true
        } 
    })
    if (check == false) {
        alert('Musíte označit aspoň jeden druh kontejneru')
        return
    }
    let url = 'api/markers'
    fetch(url, {
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        method: 'post',
        body: JSON.stringify(this.marker)
    })
        .then(async (response) => {
            if (response.status === 200) {
                this.marker = null
                cancel(19)
            } else {
                this.marker = null
                cancel()
            }
        })
        .catch((error) => {
            this.marker = null
            cancel()
        })
}

function cancel(zoom = 16) {
    editMode = false
    document.querySelector('#newMarkerButton').style.display = "block"
    document.querySelector('#markers').style.display = "block"
    document.querySelector('#newMarker').style.display = "none"
    document.querySelector('#loginInfo').style.display = "block"
    map.setMapTypeId('styled_map')
    document.querySelector("#center").style.display = "none"
    document.querySelector("#address").style.display = "none"
    map.setZoom(zoom)
}

function initMap() {
    let styledMapType = new google.maps.StyledMapType(
        [
            {
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#c7e0e6"
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    },
                    {
                        "saturation": "-100"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#779977"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#d3e7d5"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#557744"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#c7e0e6"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#b6d4ae"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#b6d4ae"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#f0eb8c"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#f0eb8c"
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "557755"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#d5d5ee"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#96c6e7"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            }
        ], { name: 'Zelená mapa' });

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        minZoom: 14,
        controlSize: 28,
        mapTypeControl: true,
        streetViewControl: false,
        center: { lat: 50.073658, lng: 14.418540 },
        options: {
            gestureHandling: 'greedy',
            fullscreenControl: false
        },

        mapTypeControlOptions: {
            mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map'],
            style: google.maps.MapTypeControlStyle.DEFAULT,
            position: google.maps.ControlPosition.TOP_LEFT
        }
    })

    map.mapTypes.set('styled_map', styledMapType)
    map.setMapTypeId('styled_map')

    setTimeout(function () { fetchMarkers(); }, 500);

    map.addListener('dragend', () => {
        fetchMarkers()
        geocodeLatLng()
    })

    map.addListener('zoom_changed', () => {
        fetchMarkers()
        geocodeLatLng()
    })
}

function geocodeLatLng() {
    if (!editMode) {
        return
    }
    const position = map.getCenter()
    const geocoder = new google.maps.Geocoder()
    geocoder.geocode({ location: position }, (results, status) => {
        this.marker.lat = position.lat()
        this.marker.lng = position.lng()
        //console.log(results)
        if (status === "OK") {
            if (results[0]) {
                //lastAddress = results[0]
                const str = results[0].formatted_address.replace(/,/g, '<br>')
                const output = `${str}`
                document.querySelector("#address").innerHTML = output
                //console.log(results[0].address_components)
                // let house
                // let street
                // let city
                // _.forEach(results[0].address_components, match => {
                //     if (_.includes(match.types, 'street_number')) {
                //         //console.log(match['long_name'])
                //         house = match['long_name']
                //     }
                //     if (_.includes(match.types, 'route')) {
                //         //console.log(match['long_name'])
                //         street = match['long_name']
                //     }
                //     if (_.includes(match.types, 'sublocality_level_1')) {
                //         //console.log(match['long_name'])
                //         city = match['long_name']
                //     } else if (_.includes(match.types, 'locality')) {
                //         //console.log(match['long_name'])
                //         city = match['long_name']
                //     }
                // })
                // this.marker.position.name = street + ' ' + house
                // this.marker.position.city = city
            } else {
                //console.log("No results found")
                document.querySelector("#address").innerHTML = "Nenalezena adresa.<br>Prosím neměňte pozici mapy rychle za sebou, <br>Google má omezení na počet požadavků v čase."

            }
        } else {
            //console.log(status)
            document.querySelector("#address").innerHTML = "Nenalezena adresa.<br>Prosím neměňte pozici mapy rychle za sebou, <br>Google má omezení na počet požadavků v čase."
        }
    })
}
