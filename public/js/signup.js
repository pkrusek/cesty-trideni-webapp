window.addEventListener("load", function () {
    const siteForm = document.getElementById("siteForm")
    siteForm.addEventListener("submit", function () {
        signUp(siteForm)
    })
})

function signUp(form) {
    if (form.psswd1.value != form.psswd2.value || form.psswd1.value.length < 8) {
        const el = document.querySelector("#responseBad")
        el.innerHTML = 'Hesla neodpovídají nebo nemají aspoň 8 znaků'
        el.style.display = "block"
        setTimeout(function () {
            el.style.display = 'none'
        }, 7000)
        return
    }
    if (!emailIsValid(form.username.value.trim())) {
        const el = document.querySelector("#responseBad")
        el.innerHTML = 'Neplatný email'
        el.style.display = "block"
        setTimeout(function () {
            el.style.display = 'none'
        }, 7000)
        return
    }
    document.getElementById("loader").style.display = "block"
    document.getElementById("btn").disabled = true
    const data = {
        email: form.username.value.trim(),
        password: form.psswd1.value,
    }
    let url = 'api/auth/clerk'
    fetch(url, {
        method: 'post',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then(async (response) => {
            document.getElementById("loader").style.display = "none"
            document.getElementById("btn").disabled = false
            if (response.status === 201) {
                const el = document.querySelector("#responseOK")
                el.innerHTML = 'Děkujeme!<br>Poslední krok:<br>Pro dokončení registrace klikněte na odkaz, který jste obdrželi do Vaší e-mailové schránky.\n'
                el.style.display = "block"
            } else {
                document.getElementById("btn").style.display = "block"
                let data = await response.json()
                const el = document.querySelector("#responseBad")
                el.innerHTML = data.message
                el.style.display = "block"
                setTimeout(function () {
                    el.style.display = 'none'
                }, 7000)
            }
        })
        .catch((error) => {
        })
}

function emailIsValid(email) {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
}