import { NextFunction, Request, Response } from 'express'
import WrongAuthenticationTokenException from '../exceptions/WrongAuthenticationTokenException'
import RequestWithUser from '../models/requestWithUser.interface'
import UserModel from '../models/user.model'
import TokenGenerator from '../utils/token.generator'

const tokenGenerator = new TokenGenerator()

async function authMiddleware(req: Request, res: Response, next: NextFunction) {
    let authHeader
    if (req.headers['authorization']) {
        authHeader = req.headers['authorization']
    } else {
        if (req.header('cookie')) {
            authHeader = req.cookies['auth-token']
        }
    }
    const token = authHeader && authHeader.split(' ')[1]
    console.log(token)
    if (!token) return next(new WrongAuthenticationTokenException()) 
    try {
        const userId = await tokenGenerator.verifyJWT(token)
        console.log(userId)
        const user = await UserModel.findById(userId)
        console.log(user)
        if (user) {
            const reqWithUser = req as RequestWithUser
            reqWithUser.user = user
            next()
        } else {
            console.log('err 2')
            next(new WrongAuthenticationTokenException())
        }
    } catch (err) {
        console.log('err 3')
        console.log(err)
        next(new WrongAuthenticationTokenException())
    }
}

export default authMiddleware
