import { Router, Request, Response, NextFunction } from 'express'
import Controller from '../../interfaces/controller.interface'
import Axios from 'axios'

class InfoController implements Controller {

    public path = '/info'
    public router = Router()

    constructor() {
        this.initializeRoutes()
    }

    private initializeRoutes() {
        this.router.get(`${this.path}`, this.info)
    }

    private info = async (req: Request, res: Response) => {
        try {
            const groupBy = (key: string | number) => (array: any[]) =>
                array.reduce((objectsByKeyValue, obj) => {
                    const value = obj[key]
                    objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj)
                    return objectsByKeyValue
                }, {})

            const { data } = await Axios({
                url: 'https://www.samosebou.cz/wp-json/samosebou/kampatri',
                method: 'get'
            })

            const groupByAlphabet = groupBy('character')
            const group = groupByAlphabet(data)
            const alphabet = Object.keys(group)
            res.render('partials/alphabet', { group: group, alphabet: alphabet })

        } catch (err) {
            res.status(500).send(err)
        }
    }
}

export default InfoController
