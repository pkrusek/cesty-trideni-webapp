import * as mongoose from 'mongoose'
import RefreshToken from './refreshToken.interface'

const tokenSchema = new mongoose.Schema(
    {
        refreshToken: {
            type: String, required: true, index: true
        },
        email: {
            type: String, required: true, index: true
        }
    }
)

const refreshToken = mongoose.model<RefreshToken>('RefreshToken', tokenSchema)

export default refreshToken
