import { Document } from 'mongoose'

interface ResetToken extends Document {
    _userId: string
    token: string
    createdAt: Date
}

export default ResetToken
