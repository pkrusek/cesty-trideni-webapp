import { Router, Request, Response, NextFunction } from 'express'
import Controller from '../../interfaces/controller.interface'
import AuthenticationService from './authentication.service'
class AuthenticationController implements Controller {
    public path = '/auth'
    public router = Router()
    private authenticationService = new AuthenticationService()

    constructor() {
        this.initializeRoutes()
    }

    private initializeRoutes() {
        this.router.post(`${this.path}/registration`, this.registration)
        this.router.post(`${this.path}/clerk`, this.clerk)
        this.router.post(`${this.path}/login`, this.login)
        this.router.post(`${this.path}/facebook`, this.facebook)
        this.router.post(`${this.path}/google`, this.google)
        this.router.get(`${this.path}/verify/:token`, this.verify)
        this.router.post(`${this.path}/verify/resend`, this.resend)
        this.router.post(`${this.path}/password`, this.password)
        this.router.post(`${this.path}/password/reset`, this.passwordReset)
        this.router.post(`${this.path}/token`, this.token)
        this.router.post(`${this.path}/logout`, this.logout)
    }

    private password = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await this.authenticationService.password(req.body.email)
            res.status(200).send(response)
        } catch (err) {
            next(err)
        }
    }

    private passwordReset = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await this.authenticationService.resetPassword(req.body.password, req.body.token)
            res.status(200).send(response)
        } catch (err) {
            next(err)
        }
    }

    private verify = async (req: Request, res: Response, next: NextFunction) => {
        try {            
            const response = await this.authenticationService.verify(req.params.token)
            if (req.useragent?.isDesktop) {
                res.redirect('/confirmation')
            } else {
                res.status(200).send(response)
            }
        } catch (err) {
            next(err)
        }
    }

    private resend = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await this.authenticationService.resend(req.body.email)
            res.status(200).send(response)
        } catch (err) {
            next(err)
        }
    }

    private registration = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await this.authenticationService.register(req.body)
            res.status(201).send(response)
        } catch (err) {
            next(err)
        }
    }

    private clerk = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await this.authenticationService.clerk(req.body)
            res.status(201).send(response)
        } catch (err) {
            next(err)
        }
    }

    private login = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await this.authenticationService.login(req.body)
            if (req.header('Device') === 'browser') {
                if (response.user.role === 'admin') {
                    res.cookie('auth-token', response.token, { path: '/', expires: new Date(Date.now() + 1000*60*60), httpOnly: true, secure: false })
                    res.send(response.user.email)
                } else {
                    res.status(403).send({'message': 'Nemáte dostatečná práva.'})
                }
            } else {
                res.send(response)
            }
        } catch (err) {
            next(err)
        }
    }

    private facebook = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await this.authenticationService.facebook(req.body.token)
            res.status(201).send(response)
        } catch (err) {
            next(err)
        }
    }

    private google = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await this.authenticationService.google(req.body.token, req.body.newsletter)
            res.status(201).send(response)
        } catch (err) {
            next(err)
        }
    }

    private token = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await this.authenticationService.token(req.body)
            res.send(response)
        } catch (err) {
            next(err)
        }
    }

    private logout = async (req: Request, res: Response, next: NextFunction) => {
        try {
            if (req.header('Device') === 'browser') {
                res.clearCookie('auth-token', { path: '/', httpOnly: true, secure: false })
                res.send('logout')
            } else {
                res.send('')
            }
        } catch (err) {
            next(err)
        }
    }
}

export default AuthenticationController
