import * as mongoose from 'mongoose'
import { v4 as uuidv4 } from 'uuid'
import VerificationToken from './verificationToken.interface'

const tokenSchema = new mongoose.Schema(
    {
        _userId: {
            type: mongoose.Schema.Types.ObjectId, required: true, ref: 'UserModel', index: true
        },
        token: {
            type: String, required: true, index: true
        },
        createdAt: {
            type: Date, required: true, default: Date.now, expires: 43200
        }
    }
)

const verificationToken = mongoose.model<VerificationToken>('VerificationToken', tokenSchema)

export default verificationToken
