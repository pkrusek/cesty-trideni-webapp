import { Document } from 'mongoose'
interface RefreshToken extends Document {
    refreshToken: string
    email: string
}

export default RefreshToken
