import * as mongoose from 'mongoose'
import { v4 as uuidv4 } from 'uuid'
import ResetToken from './resetToken.interface'

const tokenSchema = new mongoose.Schema(
    {
        _userId: {
            type: mongoose.Schema.Types.ObjectId, required: true, index: true
        },
        token: {
            type: String, required: true, index: true
        },
        createdAt: {
            type: Date, required: true, default: Date.now, expires: 43200
        }
    }
)

const resetToken = mongoose.model<ResetToken>('ResetToken', tokenSchema)

export default resetToken
