import Axios from 'axios'
import Bcrypt from 'bcrypt'
import _ from 'lodash'
import UserModel from '../../models/user.model'
import VerificationToken from './verificationToken.interface'
import VerificationTokenModel from './verificationToken.model'
import ResetToken from './resetToken.interface'
import ResetTokenModel from './resetToken.model'
import User from '../../models/user.interface'
import RefreshToken from './refreshToken.interface'
import RefreshTokenModel from './refreshToken.model'
import TokenGenerator from '../../utils/token.generator'
import CommonException from '../../exceptions/CommonException'
import UserNotVerifiedException from '../../exceptions/UserNotVerifiedException'
import UserNotFoundException from '../../exceptions/UserNotFoundException'
import TokenNotFoundException from '../../exceptions/TokenNotFoundException'
import WrongCredentialsException from '../../exceptions/WrongCredentialsException'
import WrongLoginTypeException from '../../exceptions/WrongLoginTypeException'
import UserWithThatEmailAlreadyExistsException from '../../exceptions/UserWithThatEmailAlreadyExistsException'
import Mailer from '../../utils/mailer'
import { v4 as uuidv4 } from 'uuid'

class AuthenticationService {
    public user = UserModel
    private tokenGenerator: TokenGenerator

    constructor() {
        this.tokenGenerator = new TokenGenerator()
    }

    public async register(data: any) {
        const userData = data as User
        const userExists = await this.user.exists({ email: userData.email })
        if (userExists) {
            throw new UserWithThatEmailAlreadyExistsException(userData.email)
        }
        const hash = await Bcrypt.hash(userData.password, 12)
        userData.password = hash
        return this.saveUser(userData, false)
    }

    public async clerk(user: any) {
        const userData = user as User
        // const userExists = await this.user.exists({ email: userData.email })
        // if (userExists) {
        //     throw new UserWithThatEmailAlreadyExistsException(userData.email)
        // }
        const { data } = await Axios({
            url: `https://www.ekokom.cz/api/users/?email=${userData.email}`,
            method: 'get'
        })
        if (!(data as boolean)) {
            throw new CommonException('Není možné se zaregistrovat s tímto e-mailem.')
        }

        const hash = await Bcrypt.hash(userData.password, 12)
        userData.password = hash
        userData.role = 'admin'
        return this.saveUser(userData, false)
    }

    public async verify(tokenParam: string) {
        const token = await VerificationTokenModel.findOne({ token: tokenParam })
        if (!token) throw new TokenNotFoundException('Token expiroval.')
        const user = await this.user.findOne({ _id: token._userId })
        if (!user) throw new TokenNotFoundException('Nejde najít uživatele pro tento token.')
        if (user.isVerified) throw new CommonException('Uživatel se již verifikoval.')
        user.isVerified = true
        await user.save()
        return { "user": _.pick(user, ['email']) }
    }

    public async resend(email: string) {
        const user = await this.user.findOne({ email: email })
        if (!user) throw new UserNotFoundException(email)
        if (user.isVerified) throw new CommonException('Uživatel se již verifikoval.')
        await this.sendToken(user)
        return { "message": "OK" }
    }

    public async password(email: string) {
        const user = await this.user.findOne({ email: email })
        if (!user) throw new UserNotFoundException(email)
        if (user.loginType != 'login') {
            throw new WrongLoginTypeException(user.loginType)
        }
        await this.sendResetToken(user)
        return { "message": "OK" }
    }

    public async resetPassword(password: string, resettoken: string) {
        const token = await ResetTokenModel.findOne({ token: resettoken })
        if (!token) throw new CommonException('Token vypršel. Resetujte heslo znova.')
        const user = await this.user.findOne({ _id: token._userId })
        if (!user) throw new CommonException('Uživatel nenalezen.')
        const hash = await Bcrypt.hash(password, 12)
        user.password = hash
        return this.saveUser(user, true)
    }

    public async login(data: any) {
        const userData = data as User
        const user = await this.user.findOne({ email: userData.email })
        if (!user) throw new UserNotFoundException(userData.email)
        if (user.loginType != 'login' && !user.password) {
            throw new WrongLoginTypeException(user.loginType)
        }
        if (!user.isVerified) throw new UserNotVerifiedException()
        const validPwd = await Bcrypt.compare(userData.password, user.password)
        if (!validPwd) throw new WrongCredentialsException('Špatné heslo.')
        return await this.saveUser(user)
    }

    public async facebook(accessToken: string) {
        const { data } = await Axios({
            url: 'https://graph.facebook.com/me',
            method: 'get',
            params: {
                //fields: ['id', 'email', 'name', 'first_name', 'middle_name', 'last_name', 'picture.width(720).height(720)', 'short_name'].join(','),
                fields: ['email'].join(','),
                access_token: accessToken
            }
        })
        const user = {
            email: data['email'],
            //name: data['name'],
            //familyName: data['first_name'],
            //givenName: data['last_name'],
            //picture: data['picture']['data']['url'],
            loginType: 'facebook',
            isVerified: true
        }
        return await this.saveUser(user as User)
    }

    public async google(accessToken: string, newsletter: boolean) {
        const { OAuth2Client } = require('google-auth-library')
        const client = new OAuth2Client(process.env.GOOGLE_OAUTH2)
        const ticket = await client.verifyIdToken({
            idToken: accessToken,
            audience: process.env.GOOGLE_OAUTH2
        })
        const payload = ticket.getPayload()
        let picture: string = payload['picture']
        if (picture) {
            const re = /=s96/gi;
            picture = picture.replace(re, "=s720")
        }
        const user = {
            email: payload['email'],
            name: payload['name'],
            familyName: payload['family_name'],
            givenName: payload['given_name'],
            picture: picture,
            loginType: 'google',
            isVerified: true,
            isNewsletter: newsletter
        }
        return await this.saveUser(user as User)
    }

    public async token(data: any) {
        const tokenData = data as RefreshToken
        const user = await this.user.findOne({ email: tokenData.email })
        if (!user) throw new UserNotFoundException(tokenData.email)
        const token = await RefreshTokenModel.findOne({ refreshToken: tokenData.refreshToken })
        console.log('database token:')
        console.log(token?.refreshToken)
        console.log('send token:')
        console.log(tokenData.refreshToken)
        if (token?.refreshToken === tokenData.refreshToken) {
            const token = await this.tokenGenerator.generateJWT(user._id)
            return { 'token': 'Bearer ' + token }
        } else {
            throw new TokenNotFoundException('Nepodařilo se obnovit token. Prosím, znovu se přihlašte.')
        }
    }

    private async saveUser(data: User, withToken: boolean = true) {
        const user = await this.user.findOneAndUpdate({ email: data.email }, data, {
            new: true,
            upsert: true,
            setDefaultsOnInsert: true
        })
        if (!withToken) {
            await this.sendToken(user)
            return { "user": _.pick(user, ['email', 'role']) }
        }

        const token = await this.tokenGenerator.generateJWT(user._id)
        const refreshToken = await this.refreshToken(user)
        return { 'token': 'Bearer ' + token, "refreshToken": refreshToken, "user": _.pick(user, ['email', 'role']) }
    }

    private async refreshToken(user: User) {
        const token = await RefreshTokenModel.findOne({ email: user.email })
        if (token) return token.refreshToken

        const uuid = uuidv4()
        const tokenData = { email: user.email, refreshToken: uuid } as RefreshToken
        const refreshToken = await RefreshTokenModel.create(tokenData)
        return uuid
    }

    private async sendToken(user: User) {
        const tokenCheck = await VerificationTokenModel.findOne({ _userId: user._id })
        if (tokenCheck && (new Date().valueOf() - tokenCheck.createdAt.valueOf()) < 10 * 1000) {
            throw new CommonException('Nový e-mail můžete vyžádat jednou za minutu.')
        }
        await tokenCheck?.remove()
        const tokenData = { _userId: user._id, token: uuidv4() } as VerificationToken
        const token = await VerificationTokenModel.create(tokenData)

        await new Mailer().sendRegistration(user.email, token.token)
    }

    private async sendResetToken(user: User) {
        const tokenData = { _userId: user._id, token: uuidv4() } as ResetToken
        const token = await ResetTokenModel.create(tokenData)
        await new Mailer().resetPassword(user.email, token.token)
    }
}

export default AuthenticationService
