import { Document } from 'mongoose'

interface VerificationToken extends Document {
    _userId: string
    token: string
    createdAt: Date
}

export default VerificationToken
