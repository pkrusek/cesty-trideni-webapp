import { Router, Request, Response, NextFunction } from 'express'
import Controller from '../../interfaces/controller.interface'
import RequestMarkersParams from '../../interfaces/requestMarkers.interface'
import IBBox from '../../interfaces/ibbox.interface'
import GeoHash from 'ngeohash'
import MarkerModel from '../../models/marker.model'
import AuthMiddleware from '../../middleware/auth.middleware'
import RequestWithUser from '../../models/requestWithUser.interface'

class MapsMarkersController implements Controller {

    public path = '/maps/markers'
    public router = Router()
    private markerModel = MarkerModel

    constructor() {
        this.initializeRoutes()
    }

    private initializeRoutes() {
        this.router.get(`${this.path}`, this.markers)
        //this.router.get(`${this.path}/edit`, AuthMiddleware, this.editMarkers)
        this.router.get(`${this.path}/edit`, this.editMarkers)
    }

    private editMarkers = async (req: Request, res: Response) => {
        //const reqWithUser = req as RequestWithUser
        const reqParams = this.markersParams(req.query)

        //console.log(`maps/markers?southWestLng=${reqParams.southWestLng}&southWestLat=${reqParams.southWestLat}&northEastLng=${reqParams.northEastLng}4&northEastLat=${reqParams.northEastLat}&zoom=${reqParams.zoom}`)

        try {
            const bottomLeft = [reqParams.southWestLng, reqParams.southWestLat]
            const upperRight = [reqParams.northEastLng, reqParams.northEastLat]

            const result = await this.markerModel.find().geoWithinEdit(bottomLeft, upperRight).exec()
            res.send(result)
        } catch (err) {
            res.status(500).send({ 'error': err })
        }
    }

    private markers = async (req: Request, res: Response) => {
        const reqParams = this.markersParams(req.query)
        
        //console.log(`maps/markers?southWestLng=${reqParams.southWestLng}&southWestLat=${reqParams.southWestLat}&northEastLng=${reqParams.northEastLng}4&northEastLat=${reqParams.northEastLat}&zoom=${reqParams.zoom}`)

        if (reqParams.zoom < 16) {
            const bBox: IBBox = {
                minLat: reqParams.southWestLat,
                maxLat: reqParams.northEastLat,
                minLng: reqParams.southWestLng,
                maxLng: reqParams.northEastLng
            }
            const clusters = 64
            const geoHashPrefixLength = this.getGeoHashLength(bBox, clusters)
            const geoHashPrefixes = GeoHash.bboxes(bBox.minLat, bBox.minLng, bBox.maxLat, bBox.maxLng, geoHashPrefixLength)

            const data = {
                bBox: bBox,
                geoHashPrefixLength: geoHashPrefixLength,
                geoHashPrefixes: geoHashPrefixes
            }

            try {
                const result = await this.markerModel.find().getClusters(data).exec()
                result.forEach(function (val: { count: number; pointsData: any }) {
                    if (val.count > 4) {
                        delete val.pointsData
                    }
                })
                res.send(result)
            } catch (err) {
                res.status(500).send({ 'error': err })
            }
        } else {
            try {
                const bottomLeft = [reqParams.southWestLng, reqParams.southWestLat]
                const upperRight = [reqParams.northEastLng, reqParams.northEastLat]
    
                const result = await this.markerModel.find().geoWithin(bottomLeft, upperRight).exec()
                res.send(result)
            } catch (err) {
                res.status(500).send({ 'error': err })
            }
        }
    }

    private markersParams(params: any) {
        const retVal: RequestMarkersParams = {
            southWestLng: Number(params.southWestLng),
            southWestLat: Number(params.southWestLat),
            northEastLng: Number(params.northEastLng),
            northEastLat: Number(params.northEastLat),
            zoom: Number(params.zoom)
        }
        return retVal
    }

    private getGeoHashLength(bBox: IBBox, maxClusters: number) {
        const latDiff = bBox.maxLat - bBox.minLat
        const lngDiff = bBox.maxLng - bBox.minLng

        let numberOfClusters = 0
        let geohashLength = 0
        let latInterval = 180
        let lngInterval = 360

        while (true) {

            geohashLength++

            if (geohashLength % 2 == 1) {
                latInterval = latInterval / Math.pow(2, 2)
                lngInterval = lngInterval / Math.pow(2, 3)
            } else {
                latInterval = latInterval / Math.pow(2, 3)
                lngInterval = lngInterval / Math.pow(2, 2)
            }

            var newNumberOfClusters = (latDiff / latInterval) * (lngDiff / lngInterval)

            if (newNumberOfClusters > maxClusters) {
                if (numberOfClusters <= 0) {
                    return geohashLength
                } else {
                    return geohashLength - 1
                }
            }
            numberOfClusters = newNumberOfClusters
        }
    }

}

export default MapsMarkersController
