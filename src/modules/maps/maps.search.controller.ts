import { Router, Request, Response, NextFunction } from 'express'
import Axios from 'axios'
import Controller from '../../interfaces/controller.interface'
import MarkerModel from '../../models/marker.model'

class MapsSearchController implements Controller {

    public path = '/maps/search'
    public router = Router()
    private markerModel = MarkerModel

    constructor() {
        this.initializeRoutes()
    }

    private initializeRoutes() {
        this.router.get(`${this.path}`, this.search)
        this.router.get(`${this.path}/:id`, this.searchResult)
    }

    private search = async (req: Request, res: Response) => {
        const param = encodeURIComponent(String(req.query.q))

        try {
            const { data } = await Axios({
                url: `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${param}&types=geocode&region=cz&language=cs&key=AIzaSyA5TvDb0XM15WhvlwjDb0AljXU-kNCL_Io`,
                method: 'get'
            })
            res.send(data['predictions'])
        } catch (err) {
            res.status(500).send(err)
        }
    }

    private searchResult = async (req: Request, res: Response) => {
        const placeId = req.params.id
        try {
          const { data } = await Axios({
            url: `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeId}&key=AIzaSyA5TvDb0XM15WhvlwjDb0AljXU-kNCL_Io`,
            method: 'get'
          })
          const obj = { 'lng': data['result']['geometry']['location']['lng'], 'lat': data['result']['geometry']['location']['lat'] }
          res.json([ obj.lng, obj.lat ])
        } catch (err) {
          res.status(500).send(err)
        }
      }
}

export default MapsSearchController
