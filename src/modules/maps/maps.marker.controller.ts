import { Router, Request, Response, NextFunction } from 'express'
import Controller from '../../interfaces/controller.interface'
import MarkerModel from '../../models/marker.model'
import MarkerNotFoundException from '../../exceptions/MarkerNotFoundException'

class MapsMarkerController implements Controller {

    public path = '/maps/marker'
    public router = Router()
    private markerModel = MarkerModel

    constructor() {
        this.initializeRoutes()
    }

    private initializeRoutes() {
        this.router.get(`${this.path}/closest`, this.closest)
        this.router.get(`${this.path}/:id`, this.marker)
    }

    private marker = async (req: Request, res: Response, next: NextFunction) => {
        const id = req.params.id

        try {
            const marker = await this.markerModel.findById(id)
            if (marker) {
                // const jsDateObj = new Date(marker.updatedAt)
                // const date = jsDateObj.toLocaleDateString('cs-CZ').replace(/\//g, '.')
                // const params = {
                //     cluster: marker.cluster,
                //     address: marker.position.name + ", " + marker.position.city,
                //     updated: date,
                //     location: marker.location
                // }
                // res.render('partials/infoWindow', { parameters: params })
                console.log(marker)
                res.send(marker)
            } else {
                next(new MarkerNotFoundException(id))
            }
        } catch (err) {
            next(new MarkerNotFoundException(id))
        }
    }

    private closest = async (req: Request, res: Response, next: NextFunction) => {
        const lat =  Number(req.query.lat)
        const lng =  Number(req.query.lng)
        try {
            //let query = { location: { $near: [lng, lat] }}
            let query = { location : { $near : [ lng, lat ] } } 
            let result = await this.markerModel.findOne(query)
            res.send(result)
        } catch (err) {
            console.log(err)
            next(new MarkerNotFoundException(''))
        }
    }
    
}

export default MapsMarkerController
