import { Express, Router, Request, Response, NextFunction } from 'express'
import Controller from '../../interfaces/controller.interface'
import RequestWithUser from '../../models/requestWithUser.interface'
import AuthMiddleware from '../../middleware/auth.middleware'
import Marker from '../../models/marker.interface'
import MarkerModel from '../../models/marker.model'
import GeoHash from 'ngeohash'
import MarkerNotFound from '../../exceptions/MarkerNotFoundException'
import CommonException from '../../exceptions/CommonException'
import _ from 'lodash'
import User from '../../models/user.interface'
import NodeGeocoder from 'node-geocoder'
//const markerGeocoder: any = require('./markerGeocoder')
class MarkersController implements Controller {
    public path = '/markers'
    public router = Router()

    constructor() {
        this.initializeRoutes()
    }

    private initializeRoutes() {
        this.router.get(`${this.path}/`, this.markers)
        // this.router.post(`${this.path}/`, AuthMiddleware, this.newMarker)
        // this.router.put(`${this.path}/`, AuthMiddleware, this.editMarker)
        //this.router.get(`${this.path}/:id`, AuthMiddleware, this.marker)
        // this.router.post(`${this.path}/report/:id`, AuthMiddleware, this.reportMarker)

        this.router.post(`${this.path}/`, this.newMarker)
        this.router.put(`${this.path}/`, this.editMarker)
        this.router.get(`${this.path}/:id`, this.marker)
        this.router.post(`${this.path}/report/:id`, this.reportMarker)
    }

    private markers = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const bottomLeft = [Number(req.query.southWestLng), Number(req.query.southWestLat)]
            const upperRight = [Number(req.query.northEastLng), Number(req.query.northEastLat)]
            const result = await MarkerModel.find().geoWithinAPI(bottomLeft, upperRight).exec()
            res.send(result)
        } catch (err) {
            next(err)
        }
    }

    private marker = async (req: Request, res: Response, next: NextFunction) => {
        try {
            console.log(req.params.id)
            const marker = await MarkerModel.findOne({ _id: req.params.id })
            const sendMarker = _.pick(marker, ['_id', 'lat', 'lng', 'cluster', 'position', 'isVerified', 'isHidden'])
            res.send(sendMarker)
            
            // console.log(req.params.id)
            // const reqWithUser = req as RequestWithUser
            // const marker = await MarkerModel.findOne({ _id: req.params.id })
            // const user = reqWithUser.user
            // const sendMarker = _.pick(marker, ['_id', 'lat', 'lng', 'cluster', 'position', 'isVerified', 'isHidden'])
            // if (user.role === 'public' && user._id.toString() === marker?._user._id.toString()) {
            //     next(new CommonException('Nemůžete editovat nebo potvrdit místo, jehož jste poslední editor.'))
            // } else {
            //     res.send(sendMarker)
            // }
        } catch (err) {
            next(new MarkerNotFound(req.params.id))
        }
    }

    private newMarker = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const reqWithUser = req as RequestWithUser
            let place = req.body as Marker
            const val = await this.getAddressFromLocation(place.lat, place.lng)
            const user = reqWithUser.user
            place.position = val
            const marker = await this.saveMarker(place, user)
            res.send(_.pick(marker, ['_id', 'lat', 'lng', 'isVerified']))
        } catch (err) {
            next(err)
        }
    }

    private editMarker = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const reqWithUser = req as RequestWithUser
            const user = reqWithUser.user
            const place = req.body as Marker
            const marker = await MarkerModel.findOne({ _id: place._id })
            if (marker) {
                if (user.role === 'public' && user._id.toString() === marker?._user._id.toString()) {
                    next(new CommonException('Nemůžete editovat nebo potvrdit místo, jehož jste poslední editor.'))
                } else {
                    const val = await this.getAddressFromLocation(place.lat, place.lng)
                    place.position = val
                    const editMarker = await this.changeMarker(place, marker, user)
                    res.send(_.pick(editMarker, ['_id', 'lat', 'lng', 'isVerified']))
                }
            } else {
                next(new MarkerNotFound(place._id))
            }
        } catch (err) {
            next(err)
        }
    }

    private reportMarker = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const reqWithUser = req as RequestWithUser
            const user = reqWithUser.user
            const marker = await MarkerModel.findOne({ _id: req.params.id })
            if (marker) {
                if (user.role === 'public') {
                    res.status(201).send({ 'status': 'ok'  })
                    // ceka se na schvaleni
                } else {
                    marker.isHidden = true
                    await marker.save()
                    res.send(_.pick(marker, ['_id', 'lat', 'lng', 'isHidden']))
                }
            } else {
                next(new MarkerNotFound(req.params.id))
            }
        } catch (err) {
            next(err)
        }
    }

    private async changeMarker(newMarker: Marker, origMarker: Marker, inUser: User) {
        origMarker.lat = newMarker.lat
        origMarker.lng = newMarker.lng
        origMarker.cluster = newMarker.cluster
        origMarker.position = newMarker.position
        origMarker.geoHash = GeoHash.encode(newMarker.lat, newMarker.lng, 12)
        origMarker.location = [newMarker.lng, newMarker.lat]
        //origMarker.isVerified = newMarker.isVerified
        origMarker.isVerified = inUser.role === 'public' ? !origMarker.isVerified : true
        origMarker._user = inUser
        if (inUser.role === 'admin') {
            origMarker.isHidden = false
        }
        return await origMarker.save()
    }

    private async saveMarker(inMarker: Marker, inUser: User) {
        const verified = inUser.role === 'public' ? false : true
        const marker = new MarkerModel({
            lat: inMarker.lat,
            lng: inMarker.lng,
            geoHash: GeoHash.encode(inMarker.lat, inMarker.lng, 12),
            location: [inMarker.lng, inMarker.lat],
            _user: inUser,
            cluster: inMarker.cluster,
            position: inMarker.position,
            isVerified: verified
        })
        return await marker.save()
    }

    private async getAddressFromLocation(lat: number, lng: number) {
        const options: NodeGeocoder.Options = {
            provider: 'google',
            apiKey: 'AIzaSyDy9oVwZda-37mV6WVGJ1qdVD8erpOftE8',
            language: 'cs',
            excludePartialMatches: false
        }
        
        const geocoder = NodeGeocoder(options)

        const res = await geocoder.reverse({ lat: lat, lon: lng })

        const address = res[0]

        let name = ""
        let city = ""

        if (address.city) {
            city = address.city
        } else {
            city = address.administrativeLevels?.level2long || ""
        }

        if (address.streetName) {
            name = address.streetName + " " + address.streetNumber
        } else {
            name = city
        }
        
        return { 'name': name, 'city': city }
    }
}

export default MarkersController
