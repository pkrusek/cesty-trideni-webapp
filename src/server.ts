import 'dotenv/config'
import App from './app'
import MapsMarkersController from './modules/maps/maps.markers.controller'
import MapsMarkerController from './modules/maps/maps.marker.controller'
import MapsSearchController from './modules/maps/maps.search.controller'
import AuthenticationController from './modules/authentication/authentication.controller'
import MarkersController from './modules/markers/markers.controller'
import InfoController from './modules/info/info.controller'
import validateEnv from './utils/validate.env'

validateEnv()

const app = new App(
  [
    new MapsMarkersController(),
    new MapsMarkerController(),
    new MapsSearchController(),
    new AuthenticationController(),
    new MarkersController(),
    new InfoController()
  ]
)

app.listen()
