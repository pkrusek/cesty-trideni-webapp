interface IBBox {
    minLat: number
    maxLat: number
    minLng: number
    maxLng: number
}

export default IBBox
