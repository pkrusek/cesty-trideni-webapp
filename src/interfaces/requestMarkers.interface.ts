interface RequestMarkersParams {
    southWestLng: number
    southWestLat: number
    northEastLng: number
    northEastLat: number
    zoom: number
}

export default RequestMarkersParams
