import Express from 'express'
import Helmet from 'helmet'
import csp from 'helmet-csp'
import Controller from './interfaces/controller.interface'
import MongoLoader from './utils/loader.mongo'
import errorMiddleware from './middleware/error.middleware';
import Path from 'path'
import CookieParser from 'cookie-parser'
import Useragent from 'express-useragent'

class App {
    public app: Express.Application

    constructor(controllers: Controller[]) {
        this.app = Express()
        this.app.set('etag', false)

        this.connectToTheDatabase()
        this.initializeMiddlewares()

        this.serveStatic()
        this.initializeControllers(controllers)
        this.initializeErrorHandling()
    }

    public listen() {
        this.app.listen(process.env.PORT, () => {
            console.log(`App listening on the port ${process.env.PORT}`);
        })
    }

    public getServer() {
        return this.app
    }

    private initializeMiddlewares() {
        this.app.use(Express.json())
        this.app.use(Express.urlencoded({ extended: false }))
        this.app.use(CookieParser())
        this.app.use(Useragent.express())

        this.app.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*")
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            next()
        })
    }

    private initializeErrorHandling() {
        this.app.use(errorMiddleware)
    }

    private serveStatic() {

        this.app.use(Express.static(Path.join(__dirname, "../public")))
        this.app.set('view engine', 'ejs')

        // this.app.get('/', (req, res) => {
        //     res.sendFile(Path.join(__dirname, '../public/index.html'), { root: __dirname })
        // })

        var admin = Path.join(__dirname, '../public/admin.html')
        this.app.get('/admin', (req, res) => {
            res.sendFile(admin)
        })

        var passwordReset = Path.join(__dirname, '../public/resetPassword.html')
        this.app.get('/passwordReset', (req, res) => {
            res.sendFile(passwordReset)
        })

        var password = Path.join(__dirname, '../public/password.html')
        this.app.get('/password', (req, res) => {
            res.sendFile(password)
        })

        var signup = Path.join(__dirname, '../public/signup.html')
        this.app.get('/signup', (req, res) => {
            res.sendFile(signup)
        })

        var confirmation = Path.join(__dirname, '../public/confirmation.html')
        this.app.get('/confirmation', (req, res) => {
            res.sendFile(confirmation)
        })

        this.app.get('/version', (reg, res) => {
            const pkgInfo = require('../package.json')
            res.render('partials/version', { version: pkgInfo.version })
        })

        this.app.get('/changelog', (reg, res) => {
            res.sendFile('public/changelog.html', { root: __dirname })
        })

        var aasa = Path.join(__dirname, '../.well-known/apple-app-site-association')
        this.app.get('/.well-known/apple-app-site-association', (reg, res) => {
            res.set('Content-Type', 'application/pkcs7-mime')
            res.status(200).sendFile(aasa)
        })
    }

    private initializeControllers(controllers: Controller[]) {
        controllers.forEach((controller) => {
            this.app.use('/', controller.router)
            this.app.use('/api', controller.router)
        })
    }

    private connectToTheDatabase() {
        MongoLoader()
    }
}

export default App
