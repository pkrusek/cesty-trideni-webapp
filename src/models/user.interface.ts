import { Document } from 'mongoose'

export enum LoginType {
    login = "login",
    facebook = "facebook",
    google = "google"
}
interface User extends Document {
    _id: string
    email: string
    name: string
    role: string
    familyName: string
    givenName: string
    picture: string
    password: string
    isVerified: boolean
    isAgreement: boolean
    isBlacklisted: boolean
    isNewsletter: boolean
    verifyToken: string
    loginType: LoginType
    updatedAt: Date
    createdAt: Date
}

export default User
