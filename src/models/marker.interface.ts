import { Document } from 'mongoose'
import User from './user.interface'

interface Marker extends Document {
    _id: string
    lat: number
    lng: number
    geoHash: string
    location: number[]
    cluster: {
        paper: boolean
        plastic: boolean
        glass: boolean
        drinkCarton: boolean
        metal: boolean
        courtyard: boolean
    }
    isHidden: boolean
    isVerified: boolean
    position: {
        city: string
        name: string
    }
    isEditable: boolean
    _user: User
    updatedAt: Date
    createdAt: Date
}

export default Marker
