import Mongoose from 'mongoose'
import Marker from './marker.interface'
import GeoHash from 'ngeohash'

import IBBox from '../interfaces/ibbox.interface'
import { stubFalse } from 'lodash'

interface MarkerModel extends Mongoose.Model<Marker, typeof markerQueryHelpers> {
}

const markerSchema = new Mongoose.Schema(
    {
        lat: {
            type: Number
        },
        lng: {
            type: Number
        },
        location: {
            type: [Number],
            index: '2d'
        },
        geoHash: {
            type: String,
            index: true
        },
        isHidden: {
            type: Boolean,
            default: false
        },
        isVerified: {
            type: Boolean,
            default: false
        },
        position: {
            city: String,
            name: String
        },
        cluster: {
            paper: {
                type: Boolean,
                default: false
            },    
            plastic: {
                type: Boolean,
                default: false
            },
            glass: {
                type: Boolean,
                default: false
            },
            drinkCarton: {
                type: Boolean,
                default: false
            },
            metal: {
                type: Boolean,
                default: false
            },
            courtyard: {
                type: Boolean,
                default: false
            }
        },
        _user: {
            //type: String
            type: Mongoose.Schema.Types.ObjectId, required: true, ref: 'User'
        },
        owner: {
            type: Mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    },
    { timestamps: true }
)

markerSchema.pre<Marker>('save', function (next) {
    this.location = [this.lng, this.lat]
    this.geoHash = GeoHash.encode(this.lat, this.lng, 12)
    next()
})

let markerQueryHelpers = {
    geoWithin(this: Mongoose.DocumentQuery<any, Marker>, bottomLeft: number[], upperRight: number[]) {
        return this.find({
            location: {
                $geoWithin: {
                    $box: [bottomLeft, upperRight]
                }
            }, 
            isVerified: true,
            isHidden: false
        }).select({ _id: true, lat: true, lng: true, cluster: true })
    },
    geoWithinAPI(this: Mongoose.DocumentQuery<any, Marker>, bottomLeft: number[], upperRight: number[]) {
        return this.find({
            location: {
                $geoWithin: {
                    $box: [bottomLeft, upperRight]
                }
            },
            isHidden: false
        }).select({ _id: true, lat: true, lng: true, isVerified: true, isHidden: true })
    },
    geoWithinEdit(this: Mongoose.DocumentQuery<any, Marker>, bottomLeft: number[], upperRight: number[]) {
        return this.find({
            location: {
                $geoWithin: {
                    $box: [bottomLeft, upperRight]
                }
            }
        }).sort({'position.name': 1}).select({ _id: true, lat: true, lng: true, position: true, isVerified: true, isHidden: true })
    }, 
    getClusters(data: { bBox: IBBox; geoHashPrefixLength: number; geoHashPrefixes: string[] }) {
        return markerModel.aggregate(
            [
                {
                    $project: {
                        _id: 1,
                        lat: 1,
                        lng: 1,
                        cluster: 1,
                        geoHash: 1,
                        isVerified: 1,
                        isHidden: 1,
                        geohashPrefix: {
                            $substr: ['$geoHash', 0, data.geoHashPrefixLength]
                        }
                    }
                },
                {
                    $match: {
                        geohashPrefix: { $in: data.geoHashPrefixes },
                        isVerified: true,
                        isHidden: false
                    }
                },
                // {
                //   // Ensure all resulting points are within the bounding box.
                //   $match: {
                //     lat: { $gte: data.bBox.minLat, $lte: data.bBox.maxLat },
                //     lng: { $gte: data.bBox.minLng, $lte: data.bBox.maxLng }
                //   }
                // },
                {
                    $group: {
                        _id: '$geohashPrefix',
                        count: { $sum: 1 },
                        lat: { $avg: '$lat' },
                        lng: { $avg: '$lng' },
                        "pointsData": {
                            $push: {
                                _id: "$_id",
                                lat: "$lat",
                                lng: "$lng",
                                cluster: "$cluster"
                            }
                        }
                    }
                }
            ]
        )
    }
}

markerSchema.query = markerQueryHelpers

const markerModel = Mongoose.model<Marker, MarkerModel>('Marker', markerSchema)

export default markerModel
