import * as Mongoose from 'mongoose'
import User from './user.interface'

const userSchema = new Mongoose.Schema(
    {
        email: {
            type: String,
            lowercase: true,
            unique: true,
            trim: true,
            required: true,
            index: true
        },
        password: {
            type: String
        },
        loginType: {
            type: String,
            enum : ['login','facebook', 'google'],
            default: 'login'
        },
        role: {
            type: String,
            enum : ['public','editor', 'admin'],
            default: 'public'
        },
        name: {
            type: String,
            trim: true
        },
        givenName: {
            type: String,
            trim: true
        },
        familyName: {
            type: String,
            trim: true
        },
        picture: {
            type: String
        },
        isVerified: {
            type: Boolean,
            default: false
        },
        isBlacklisted: {
            type: Boolean,
            default: false
        },
        isNewsletter: {
            type: Boolean,
            default: false
        },
        markersOwner: [{
            type: Mongoose.Schema.Types.ObjectId,
            ref: 'Marker'
        }]
    },
    { timestamps: true }
)

const userModel = Mongoose.model<User>('User', userSchema)

export default userModel
