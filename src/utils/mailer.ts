import nodemailer, { Transporter } from 'nodemailer'

class Mailer {
    private transporter: Transporter

    constructor() {
        const transport = {
            host: 'smtp.eu.mailgun.org',
            port: 587,
            auth: {
                user: 'postmaster@kam-tridit.lab-ad.cz',
                pass: '78f87acf63c8dd3962eb59136c826ced-0f472795-e3d1de80'
            }
        }
        this.transporter = nodemailer.createTransport(transport)
    }

    public sendRegistration(recipient: string, token: string) {
        const mail = {
            from: 'postmaster@kam-tridit.lab-ad.cz',
            to: recipient,
            subject: 'Děkujeme!',
            html: `
            Vítejte v aplikaci Kam třídit, posledním krokem pro dokončení registrace je ověření vaší e-mailové adresy.
            <br> 
            Pro verifikaci vašeho e-mailu <a href="https://kam-tridit.lab-ad.cz/api/auth/verify/${token}">klikněte zde.</a>
            `
        }
        return this.transporter.sendMail(mail)
    }

    public resetPassword(recipient: string, token: string) {
        const mail = {
            from: 'postmaster@kam-tridit.lab-ad.cz',
            to: recipient,
            subject: 'Reset hesla',
            html: `
            Pro reset hesla klikněte na odkaz:
            <br><br>

            <a href="https://kam-tridit.lab-ad.cz/passwordReset?token=${token}">reset hesla</a>
            `
        }
        return this.transporter.sendMail(mail)
    }
}

export default Mailer
