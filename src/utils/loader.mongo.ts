import mongoose from 'mongoose'
import { Db } from 'mongodb'

export default async (): Promise<Db> => {
    try {
        const conn = await mongoose.connect(process.env.DATABASE_CONN || '', {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        })
        console.log(`MongoDB Connected: ${conn.connection.host}`)
        return conn.connection.db
    } catch (err) {
        console.error(err)
        process.exit(1)
    }
}
