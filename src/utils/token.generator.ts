import jwt from 'jsonwebtoken'
import fs from 'fs'
import path from 'path'
import DataStoredInToken from '../models/dataInJWT.interface'

class TokenGenerator {
    private publicKey: string
    private privateKey: string

    constructor() {
        this.publicKey = process.env.PUBLIC_KEY || ''
        this.privateKey = process.env.PRIVATE_KEY || ''
    }

    public generateJWT(userId: string) {
        const _id = userId
        const expiresIn = '1h'
        const payload = {
            sub: _id,
            iss: 'kamtridit.cz'
        }
        return new Promise((resolve, reject) => {
            jwt.sign(payload, this.privateKey, { expiresIn: expiresIn, algorithm: 'RS256' }, (err, token) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(token)
                }
            })
        })
    }

    public verifyJWT(token: string) {
        return new Promise((resolve, reject) => {
            const publicKey = fs.readFileSync(path.resolve(__dirname, './id_rsa_pub.pem'), 'utf8')
            jwt.verify(token, publicKey, { algorithms: ['RS256'] }, (err, decoded) => {
                if (err) {
                    reject(err)
                } else {
                    const dataStoredInToken = decoded as DataStoredInToken
                    resolve(dataStoredInToken.sub)
                }
            })
        })
    }
}

export default TokenGenerator
