import { cleanEnv, port, str } from 'envalid'

function validateEnv() {
    cleanEnv(process.env, {
        PORT: port(),
        DATABASE_CONN: str(),
        PUBLIC_KEY: str(),
        PRIVATE_KEY: str(),
        EMAIL_LOGIN: str(),
        EMAIL_PASSWORD: str()
    })
}

export default validateEnv
