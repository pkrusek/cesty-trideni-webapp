import HttpException from './HttpException'

class TokenNotFoundException extends HttpException {

  constructor(message: string) {
    super(404, message)
  }
}

export default TokenNotFoundException
