import HttpException from './HttpException'

class WrongCredentialsException extends HttpException {
  constructor(message: string) {
    super(403, message)
  }
}

export default WrongCredentialsException
