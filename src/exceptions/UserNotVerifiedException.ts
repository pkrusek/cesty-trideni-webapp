import HttpException from './HttpException'

class UserNotVerifiedException extends HttpException {

  constructor() {
    super(423, 'Prosím verifikujte svůj účet pomocí právě zaslaného emailu.')
  }
}

export default UserNotVerifiedException