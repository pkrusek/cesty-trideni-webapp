import HttpException from './HttpException'

class UserWithThatEmailAlreadyExistsException extends HttpException {

  constructor(email: string) {
    super(400, `Uživatel s e-mailem ${email} již existuje.`);
  }
}

export default UserWithThatEmailAlreadyExistsException
