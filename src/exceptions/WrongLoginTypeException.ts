import HttpException from './HttpException'

class WrongLoginTypeException extends HttpException {

  constructor(loginType: string) {
    super(400, `V minulosti jste se již přihlásili přes ${loginType}.`)
  }
}

export default WrongLoginTypeException
