import HttpException from './HttpException'

class SystemException extends HttpException {

  constructor() {
    super(500, `Chyba na serveru, zkuste prosím později.`)
  }
}

export default SystemException
