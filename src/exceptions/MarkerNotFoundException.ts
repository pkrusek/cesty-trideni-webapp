import HttpException from './HttpException';

class MarkerNotFoundException extends HttpException {

  constructor(id: string) {
    super(404, `Marker with id ${id} not found`)
  }
}

export default MarkerNotFoundException
