import HttpException from './HttpException'

class UserNotFoundException extends HttpException {

  constructor(email: string) {
    super(404, `Uživatel s e-mailem ${email} nenalezen`)
  }
}

export default UserNotFoundException
