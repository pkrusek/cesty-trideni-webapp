import HttpException from './HttpException'

class WrongAuthenticationTokenException extends HttpException {

  constructor() {
    super(401, 'Problém s autorizací, prosím přihlašte se znovu.')
  }
}

export default WrongAuthenticationTokenException
