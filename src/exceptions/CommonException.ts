import HttpException from './HttpException'

class CommonException extends HttpException {

  constructor(message: string) {
    super(400, message)
  }
}

export default CommonException