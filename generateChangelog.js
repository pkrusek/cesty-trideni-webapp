var marked = require('marked');
var fs = require('fs');

var readMe = fs.readFileSync('CHANGELOG.md', 'utf-8');
var markdownReadMe = marked(readMe);

fs.writeFileSync('./public/CHANGELOG_GENERATED.html', markdownReadMe);